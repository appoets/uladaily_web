@extends('user.layouts.app')

@section('content')
@include('user.layouts.partials.user_common')

<div class="container">
	<div class="">
		@include('user.layouts.partials.sidebar')

		<!-- Tab panes -->
		<div class="tab-content mb-5">
			<div class="tab-pane container active" id="orders">
				<div class="store-list">
					<h4>@lang('user.create.ongoing_orders')</h4>

					@forelse($Ongoing as $Index => $Order)

					<div class="row">
						<div class="col-md-6">
							<!-- Order Box Starts -->
							<?php //dd($Order); ?>



							<div class="order-box box-shadow px-4 py-3 align-items-center">
								<div class="store-img text-center">
									<img src="{{$Order->shop->avatar}}" class="img-fluid" width="70%;">
								</div>

								<div class="order-details w-100">
									<div class="pb-3 pt-1">
										<h5>{{$Order->shop->name}}</h5>
										<p>{{$Order->shop->maps_address}}</p>
										<p>Otp:- {{$Order->order_otp}}</p>

									</div>
									<div class="order-bottom pt-3 align-items-center">
										<div class="float-right">
											<a class="btn btn-green-outline view-details" href="/orders/{{$Order->id}}">Details</a>
										</div>
										<div class="float-left order-btm-text">
											<p>Order Type: Delivery</p>
											<p>@lang('user.create.orders') {{$Order->id}}| {{$Order->updated_at}}</p>
										</div>
									</div>
								</div>
							</div>
						</div>




						<div class="col-md-6">
							<div class="order-box box-shadow px-4 py-3 align-items-center">
								<div class="store-img text-center">
									<img src="{{$Order->shop->avatar}}" class="img-fluid" width="70%;">
								</div>
								<div class="order-details w-100">
									<div class="pb-3 pt-2">
										<h5>{{$Order->shop->name}}</h5>
										<p>{{$Order->shop->maps_address}}</p>
									</div>
									@forelse($Order->items as $Index => $Item)
									<span>{{$Item->product->name}} x {{$Item->quantity}}, </span>
									@empty
									@endforelse
									<div class="order-bottom pt-3 align-items-center">
										<div class="float-right">
											<form action="{{url('/reorder')}}" method="POST">
												{{csrf_field()}}
												<input type="hidden" value="{{$Order->id}}" name="order_id" />
												<input type="hidden" value="{{$Order->address->id}}"
													name="user_address_id" />
												<button class="btn btn-green">@lang('user.create.reorder')</button>
											</form>
										</div>
										<div class="float-left order-btm-text">
											<p>Order Type: Delivery</p>
											<p>Thursday, 19th March 2020</p>
										</div>


										<!-- Order Box Bottom Right Starts -->
										<div class="order-box-btm-left col-md-3 col-sm-3 col-xs-12 text-right">
											<p class="total-txt">@lang('user.create.total')
												{{currencydecimal($Order->invoice->net)}}</p>
											@if($Order->status=='COMPLETED')
											@if(!$Order->has('reviewrating'))

											<a href="javascript:void(0)" data-id="{{$Order->id}}"
												data-imgshop="{{@$Order->shop->avatar}}"
												data-imgboy="{{@$Order->transporter->avatar}}"
												data-shopname="{{@$Order->shop->name}}"
												data-boyname="{{@$Order->transporter->name}}" data-rateshop="0"
												data-rateboy="0"
												class="rate-btn  ratingreview">@lang('user.create.ratings')</a>
											@else
											@if(@$Order->reviewrating->shop_id == 0 ||
											@$Order->reviewrating->transporter_id == 0)
											<a href="javascript:void(0)" data-id="{{$Order->id}}"
												data-imgshop="{{@$Order->shop->avatar}}"
												data-imgboy="{{@$Order->transporter->avatar}}"
												data-shopname="{{@$Order->shop->name}}"
												data-boyname="{{@$Order->transporter->name}}"
												data-rateshop="{{@$Order->reviewrating->shop_id?:0}}"
												data-rateboy="{{@$Order->reviewrating->transporter_id?:0}}"
												class="rate-btn  ratingreview">@lang('user.create.ratings')</a>

											@endif

											@endif
											@endif
										</div>



									</div>
								</div>
							</div>
						</div>

					</div>

					<!-- Order Box Ends -->
					@empty
					<div>@lang('user.create.no_order_found')!</div>
					@endforelse
				</div>

				<!-- past porders -->
				<div class="store-list">
					<h4>@lang('user.create.past_order')</h4>

					@forelse($Orders as $Index => $Order)

					<div class="row">
						<div class="col-md-6">
							<!-- Order Box Starts -->
							<?php //dd($Order); ?>



							<div class="order-box box-shadow px-4 py-3 align-items-center">
								<div class="store-img text-center">
									<img src="{{$Order->shop->avatar}}" class="img-fluid" width="70%;">
								</div>

								<div class="order-details w-100">
									<div class="pb-3 pt-1">
										<h5>{{$Order->shop->name}}</h5>
										<p>{{$Order->shop->maps_address}}</p>
										<p>Otp:- {{$Order->order_otp}}</p>

									</div>
									<div class="order-bottom pt-3 align-items-center">
										<div class="float-right">
										<a class="btn btn-green-outline view-details" href="/orders/{{$Order->id}}">Details</a>
										</div>
										<div class="float-left order-btm-text">
											@if($Order->status=='COMPLETED')
											<p>@lang('user.create.delivered_on') {{$Order->updated_at}}</p>
											@endif
										</div>
									</div>
								</div>
							</div>
						</div>




						<div class="col-md-6">
							<div class="order-box box-shadow px-4 py-3 align-items-center">
								<div class="store-img text-center">
									<img src="{{$Order->shop->avatar}}" class="img-fluid" width="70%;">
								</div>
								<div class="order-details w-100">
									<div class="pb-3 pt-2">
										<h5>{{$Order->shop->name}}</h5>
										<p>{{$Order->shop->maps_address}}</p>
									</div>
									@forelse($Order->items as $Index => $Item)
									<span>{{$Item->product->name}} x {{$Item->quantity}}, </span>
									@empty
									@endforelse
									<div class="order-bottom pt-3 align-items-center">
										<div class="float-right">
											<form action="{{url('/reorder')}}" method="POST">
												{{csrf_field()}}
												<input type="hidden" value="{{$Order->id}}" name="order_id" />
												<input type="hidden" value="{{$Order->address->id}}"
													name="user_address_id" />
												<button class="btn btn-green">@lang('user.create.reorder')</button>
											</form>
										</div>
										<div class="float-left order-btm-text">
											<p>Order Type: Delivery</p>
											<p>Thursday, 19th March 2020</p>
										</div>


										<!-- Order Box Bottom Right Starts -->
										<div class="order-box-btm-left col-md-3 col-sm-3 col-xs-12 text-right">
											<p class="total-txt">@lang('user.create.total')
												{{currencydecimal($Order->invoice->net)}}</p>
											@if($Order->status=='COMPLETED')
											@if(!$Order->has('reviewrating'))

											<a href="javascript:void(0)" data-id="{{$Order->id}}"
												data-imgshop="{{@$Order->shop->avatar}}"
												data-imgboy="{{@$Order->transporter->avatar}}"
												data-shopname="{{@$Order->shop->name}}"
												data-boyname="{{@$Order->transporter->name}}" data-rateshop="0"
												data-rateboy="0"
												class="rate-btn  ratingreview">@lang('user.create.ratings')</a>
											@else
											@if(@$Order->reviewrating->shop_id == 0 ||
											@$Order->reviewrating->transporter_id == 0)
											<a href="javascript:void(0)" data-id="{{$Order->id}}"
												data-imgshop="{{@$Order->shop->avatar}}"
												data-imgboy="{{@$Order->transporter->avatar}}"
												data-shopname="{{@$Order->shop->name}}"
												data-boyname="{{@$Order->transporter->name}}"
												data-rateshop="{{@$Order->reviewrating->shop_id?:0}}"
												data-rateboy="{{@$Order->reviewrating->transporter_id?:0}}"
												class="rate-btn  ratingreview">@lang('user.create.ratings')</a>

											@endif

											@endif
											@endif
										</div>



									</div>
								</div>
							</div>
						</div>

					</div>

					<!-- Order Box Ends -->
					@empty
					<div>@lang('user.create.no_order_found')!</div>
					@endforelse
				</div>


			</div>
		</div>
	</div>
</div>
</div>
@endsection