@extends('user.layouts.app')

@section('content')
@include('user.layouts.partials.user_common')

<div class="container">
	<div class="">
		@include('user.layouts.partials.sidebar')
		<div class="tab-content mb-5">
			<div class="tab-pane container active" id="offers">
				<div class="row">
					<div class="offer-img text-center pb-2">
						<img src="assets/images/offer-img.png" class="img-fluid"
							style="width:70%; border-radius: 10px;">
					</div>
					<div class="text-center mx-auto pt-3">
						<a class=" btn btn-green" href="#">View More</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection