@extends('user.layouts.app')

@section('content')
<div class="tab-content mb-5">
    <div class="tab-pane container active" id="orders">


        <div class="container order-details-box">
            <div class="row pb-3">
                <ol class="progtrckr" data-progtrckr-steps="5">
                    <li class="progtrckr-done ORDERED @if($Order->status == 'ORDERED') @endif">
                        @lang('user.create.order_created')</li>
                    <li class=" RECEIVED @if($Order->status == 'RECEIVED')  @endif ">
                        @lang('user.create.order_received')</li>
                    <li class="  PROCESSING @if($Order->status == 'PROCESSING')  @endif">
                        @lang('user.create.order_confirmed')</li>
                    <li class=" COMPLETED @if($Order->status == 'COMPLETED')  @endif ">
                        @lang('user.create.order_delivered')</li>
                </ol>
            </div>

            <div class="row pt-5">
                <div class="col-md-6">
                    <div class="order-details mr-5">
                        <div class="d-block">
                            <div class="store-img text-center">
                                <img src="{{$Order->shop->avatar}}" class="img-fluid" width="70%;">
                            </div>
                            <div class="order-details w-100">
                                <div class="pb-3 pt-1">
                                    <h5>@lang('user.create.order') #{{$Order->id}}</h5>
                                    <p>{{$Order->shop->name}}</p>
                                    <p>{{date('h:i A')}} | {{count($Order->items)}} Items |
                                        {{currencydecimal($Order->invoice->payable)}}</p>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped">
                            <tr>
                                <thead>
                                    <th>Product</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                </thead>
                            </tr>
                            <?php $tot_gross=0;?>
                            @forelse($Order->items as $item)
                            <?php $tot_gross += $item->quantity*$item->product->prices->orignal_price;  ?>
                            <tr>
                                <td>{{$item->product->name}}</td>
                                <td>{{$item->quantity}}</td>
                                <td>{{currencydecimal($item->quantity*$item->product->prices->orignal_price)}}</td>
                            </tr>
                            @empty
                            @endforelse
                        </table>

                        <h4 class="py-4">Totals</h4>

                        <table class="table table-bordered">
                            <tr>
                                <th>@lang('user.create.item_total')</th>
                                <td>{{currencydecimal($tot_gross)}}</td>
                            </tr>
                             <tr>
                                <th>Discount</th>
                                <td>-{{currencydecimal($Order->invoice->discount)}}</td>
                            </tr>
                             <tr>
                                <th>Tax</th>
                                <td>{{currencydecimal($Order->invoice->tax)}}</td>
                            </tr>

                            <tr>
                                <th>Delivery Charge</th>
                                <td>{{currencydecimal($Order->invoice->delivery_charge)}}</td>
                            </tr>

                            <!-- <tr>
                                <th>@lang('user.create.tax')(15% GST)</th>
                                <td>{{currencydecimal($Order->invoice->tax)}}</td>
                            </tr>
                            <tr>
                                <th>@lang('user.create.delivery_charges')</th>
                                <td>{{currencydecimal(Setting::get('delivery_charge'))}}</td>
                            </tr> -->
                            @if($Order->invoice->promocode_id)
                            <tr>
                                <th>@lang('user.create.promocode_discount')</th>
                                <td>{{currencydecimal($Order->invoice->promocode_amount)}}</td>
                            </tr>
                            @endif
                            @if($Order->invoice->wallet_amount)
                            <tr>
                                <th>@lang('user.create.wallet_amount')</th>
                                <td>{{currencydecimal($Order->invoice->wallet_amount)}}</td>
                            </tr>
                            @endif
                            <tr>
                                <th>@lang('user.create.to_pay')</th>
                                <td>{{currencydecimal($Order->invoice->payable)}}</td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="" id="my_map" style="width: 100%; height: 400px;"></div>
                    <form action="{{url('restaurants')}}" id="my_map_form_current">
                        <input type="hidden" id="pac-input_cur" class="form-control search-loc-form"
                            placeholder="Search for area,street name..." name="search_loc"
                            value="{{ old('latitude') }}">
                        <input type="hidden" id="latitude" name="latitude" value="{{$Order->shop->latitude}}" readonly>
                        <input type="hidden" id="longitude" name="longitude" value="{{$Order->shop->longitude}}"
                            readonly>

                    </form>
                    <!-- <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387190.2798911863!2d-74.25986818535776!3d40.69767006766623!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew%20York%2C%20NY%2C%20USA!5e0!3m2!1sen!2sin!4v1586420167431!5m2!1sen!2sin"
                        width="600" height="450" frameborder="0" style="border:0;" class="box-shadow br-10"
                        allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> -->
                </div>

                <div class="col-md-12 py-5 text-center">
                    <a href="{{url('orders')}}" class="btn btn-green mr-2">Back</a> <a href="#"
                        class="btn btn-green-outline mr-2">Cancel Order</a>
                </div>
            </div>
        </div>
    </div>
    <div id="container"></div>
         <input type="button" id="routebtn" value="route" style="display:none"  />
    @endsection

    @section('styles')
    <style type="text/css">
        /* .active {
            background-color: Pink;
            :
        } */
    </style>
    @endsection
    @section('scripts')

    @if($Order->status == 'CANCELLED' || $Order->status == 'COMPLETED')
    <script type="text/javascript">
        window.onload = function () {
            setTimeout(function () {
                $('#routebtn').trigger('click');
            }, 3000);

        };
    </script>
    @else

    <script src="https://cdnjs.cloudflare.com/ajax/libs/react/0.13.3/react.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/react/0.13.3/JSXTransformer.js"></script>

    <script type="text/jsx">
        var order = '';  var curstatus = '';
        var MainComponent = React.createClass({
            getInitialState: function () {
                    return {data: [], currency : "{{Setting::get('currency')}}"};
                },
            componentDidMount: function(){
                $.ajax({
                  url: "{{url('track/order/'.Request::segment(2))}}",
                  type: "GET"})
                  .done(function(response){

                        this.setState({
                            data:response.data
                        });

                    }.bind(this));

                    setInterval(this.checkRequest, 5000);
            },
            checkRequest : function(){
                $.ajax({
                  url: "{{url('track/order/'.Request::segment(2))}}",
                  type: "GET"})
                  .done(function(response){
                  
                        this.setState({
                            data:response
                        });

                    }.bind(this));
            },
            render: function(){
                return (
                    <div>
                        <SwitchState checkState={this.state.data} currency={this.state.currency} />
                    </div>
                );
            }
        });

        

        var SwitchState = React.createClass({

            componentDidMount: function() {
                this.changeLabel;
            },

            changeLabel : function(){
                if(this.props.checkState == undefined){
                   // window.location.reload();
                }else if(this.props.checkState != ""){ 
                    order = this.props.checkState;
                    if(curstatus != this.props.checkState.status){
                     $('#routebtn').trigger('click');
                     curstatus = this.props.checkState.status;
                    }
                    $('.progtrckr-todo').removeClass('progtrckr-done');
                    $("."+this.props.checkState.status).addClass("progtrckr-done");
                    if(this.props.checkState.status=='COMPLETED'){
                        $('#rating_review').show();
                    }else{
                        $('#rating_review').hide();
                    }
                    setTimeout(function(){
                        //$('.rating').rating();
                    },400);

                }else{
                    $("#ride_status").text('Text will appear here');
                }
            },
            render: function(){

               

                    this.changeLabel();
                   
                    return ( 
                        <p></p>
                     );
                
            }
        });
        React.render(<MainComponent/>,document.getElementById("container"));

    @endif
    </script>


    @endsection