@extends('user.layouts.app')
@section('content')
<section class="profile-baner">
   <!--  <div class="container">
        <div class="row">
            <div class="mx-auto pt-5"> -->
                <!-- <h4>Select Store for <span class="text-green"> Delivery in 500</span></h4> -->
                <!-- Intro Banner Starts -->
            <div class="intro-banner-outer section">
                <div class="container-fluid mb-0">
                    <h1 class="hero-heading">Get fresh &amp; local products from your near by stores! </h1>
                    <div class="intro-slide">
                        @forelse($BannerImage as $Shop)
                        <!-- Intro Box Starts -->
                            @if($Shop->status == 'active')
                                    @if($Shop->shopstatus=='OPEN')

                                    <a href="{{url('/restaurant/details')}}?name={{$Shop->shop->name}}" class="intro-box">
                                     <div class="intro-banner-img bg-img" style="background-image: url({{$Shop->url}});"></div>
                                    </a>
                                    @else
                                    <div class="slick-list">
                                        <a href="#" class="">
                                            <div class="intro-banner-img bg-img" style="background-image: url({{$Shop->url}});"></div>
                                            @if($Shop->shopstatus=='CLOSED')
                                                <div class="red centered"><div class="text">  @lang('user.closed')</div><div class="opentext">({{$Shop->shopopenstatus}})</div></div>
                                            @endif
                                        </a>
                                    </div>
                                    @endif    
                            @endif
                        <!-- Intro Box Ends -->
                        @empty
                        <!-- Intro Box Starts -->
                        <a href="" class="intro-box">
                            <div class="intro-banner-img bg-img" style="background-image: url({{ asset('assets/user/img/banner-2.jpg')}});"></div>
                        </a>
                        <!-- Intro Box Ends -->
                        @endforelse                        
                        
                    </div>
                </div>
            </div>
            <!-- Intro Banner Ends -->
           <!--  </div>
        </div>
    </div> -->
</section>

<section class="portfolio section">
    <div class="container">
        <div class="filter-top">
            <h6>Filtered By :</h6>
            <div class="filters">
                <ul>
                    <li class="active" data-filter="*">All</li>
                    <li data-filter=".BiggestSavings">Biggest Savings</li>
                    <li data-filter=".groceries">Groceries</li>
                    <li data-filter=".agency">Agency</li>
                    <!-- <li data-filter=".pharmacy">Pharmacy</li>
                    <li data-filter=".alchocol">Alchocol</li> -->
                    <li data-filter=".meals">Meals</li>
                </ul>
            </div>
        </div>

        <div class="filters-content">
            <div class="row grid">
            @forelse(@$Shops as $Shop)
                   
                <div class="col-sm-3 all groceries filter-item">
                    <div class="item">
                        <div class="overlay">
                        @if($Shop->shopstatus=='OPEN')

                            <h6 class="overlay-btn"><a href="{{url('/restaurant/details')}}?name={{$Shop->name}}">View</a></h6>
                        @else
                            <h6 class="overlay-btn"><a href="#">View</a></h6>
                        @endif
                        @if($Shop->shopstatus=='CLOSED')
                            <div class="red centered"><div class="text"> Closed</div><div class="opentext"> ({{$Shop->shopopenstatus}})</div></div>
                        @endif
                        </div>
                        <img src="{{$Shop->avatar}}" class="img-fluid">
                        <div class="p-inner">
                            <h5>{{$Shop->name}}</h5>
                            <div class="cat">{{$Shop->description}}</div>
                        </div>
                    </div>
                  
                </div>
            @empty
                <div>@lang('user.no_records')</div>
            @endforelse
                <!-- <div class="col-sm-3 all groceries">
                    <div class="item">
                        <div class="overlay">
                            <h6 class="overlay-btn"><a href="store-view.php">View</a></h6>
                        </div>
                        <img src="assets/images/store.png" class="img-fluid">
                        <div class="p-inner">
                            <h5>Store Title</h5>
                            <div class="cat">This is Dummy title of the above store</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 all agency">
                    <div class="item">
                        <div class="overlay">
                            <h6 class="overlay-btn"><a href="store-view.php">View</a></h6>
                        </div>
                        <img src="assets/images/store.png" class="img-fluid">
                        <div class="p-inner">
                            <h5>Store Title</h5>
                            <div class="cat">This is Dummy title of the above store</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 all pharmacy">
                    <div class="item">
                        <div class="overlay">
                            <h6 class="overlay-btn"><a href="store-view.php">View</a></h6>
                        </div>
                        <img src="assets/images/store.png" class="img-fluid">
                        <div class="p-inner">
                            <h5>Store Title</h5>
                            <div class="cat">This is Dummy title of the above store</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 all alchocol">
                    <div class="item">
                        <div class="overlay">
                            <h6 class="overlay-btn"><a href="store-view.php">View</a></h6>
                        </div>
                        <img src="assets/images/store.png" class="img-fluid">
                        <div class="p-inner">
                            <h5>Store Title</h5>
                            <div class="cat">This is Dummy title of the above store</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 all meals">
                    <div class="item">
                        <div class="overlay">
                            <h6 class="overlay-btn"><a href="store-view.php">View</a></h6>
                        </div>
                        <img src="assets/images/store.png" class="img-fluid">
                        <div class="p-inner">
                            <h5>Store Title</h5>
                            <div class="cat">This is Dummy title of the above store</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 all groceries">
                    <div class="item">
                        <div class="overlay">
                            <h6 class="overlay-btn"><a href="store-view.php">View</a></h6>
                        </div>
                        <img src="assets/images/store.png" class="img-fluid">
                        <div class="p-inner">
                            <h5>Store Title</h5>
                            <div class="cat">This is Dummy title of the above store</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 all BiggestSavings">
                    <div class="item">
                        <div class="overlay">
                            <h6 class="overlay-btn"><a href="store-view.php">View</a></h6>
                        </div>
                        <img src="assets/images/store.png" class="img-fluid">
                        <div class="p-inner">
                            <h5>Store Title</h5>
                            <div class="cat">This is Dummy title of the above store</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 all alchocol">
                    <div class="item">
                        <div class="overlay">
                            <h6 class="overlay-btn"><a href="store-view.php">View</a></h6>
                        </div>
                        <img src="assets/images/store.png" class="img-fluid">
                        <div class="p-inner">
                            <h5>Store Title</h5>
                            <div class="cat">This is Dummy title of the above store</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 all meals">
                    <div class="item">
                        <div class="overlay">
                            <h6 class="overlay-btn"><a href="store-view.php">View</a></h6>
                        </div>
                        <img src="assets/images/store.png" class="img-fluid">
                        <div class="p-inner">
                            <h5>Store Title</h5>
                            <div class="cat">This is Dummy title of the above store</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 all pharmacy">
                    <div class="item">
                        <div class="overlay">
                            <h6 class="overlay-btn"><a href="store-view.php">View</a></h6>
                        </div>
                        <img src="assets/images/store.png" class="img-fluid">
                        <div class="p-inner">
                            <h5>Store Title</h5>
                            <div class="cat">This is Dummy title of the above store</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 all agency">
                    <div class="item">
                        <div class="overlay">
                            <h6 class="overlay-btn"><a href="store-view.php">View</a></h6>
                        </div>
                        <img src="assets/images/store.png" class="img-fluid">
                        <div class="p-inner">
                            <h5>Store Title</h5>
                            <div class="cat">This is Dummy title of the above store</div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>

    </div>
</section>


@endsection