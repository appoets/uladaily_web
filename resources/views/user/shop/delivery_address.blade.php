@extends('user.layouts.app')

@section('content')
<section>
    <div class="container">
        <div class="row pt-5">
            <div class="col-md-6">
                <ul class="nav nav-pills justify-content-center py-4 mb-5">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="pill" href="#delivery">
                            <input type="radio" hidden name="">Delivery
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="pill" href="#takeaway">
                            <input type="radio" hidden name="">Takeaway
                        </a>
                    </li>
                </ul>

                <div class="tab-content mb-5">
                    <div class="tab-pane container active" id="delivery">
                        <h5>Date & Time</h5>
                        <form class="pt-2">
                            <div class="form-group">
                                <input type="date" class="form-control" name="">
                            </div>
                            <div class="form-group">
                                <input type="time" class="form-control" name="">
                            </div>
                        </form>
                        <div class="address">
                            <h5>Delivery Address</h5>
                              <!-- Address Box Ends -->
                              <?php $add_type = ['home'=> 'Home','work'=> 'Work','other'=> 'Other'];
                                    $delivery_id =0; ?>
                            @forelse(Auth::user()->addresses  as $k=>$address)
                                    <?php
                                        $delivery_addr_id = $address->id;
                                        if(in_array($address->type, $add_type)){ 
                                            if($address->type=='other'){
                                            }else{
                                                unset($add_type[$address->type]);
                                            }
                                        }
                                    ?>
                            <div class="col-md-12 my-3 p-0">
                            <!-- <a href="javascript:void(0);" data-id="{{$address->id}}" class="address-box address-cmn-box row m-0 update_addr"> -->

                                <input type="radio" name="address-check" id="address_{{$address->id}}"/>
                                <label for="address_{{$address->id}}" data-id="{{$address->id}}" class="radio update_addr">
                                    <!-- <i class="fa fa-check select-item"></i> -->
                                    <div class="address-box p-3 br-10 box-shadow box">
                                    <input type="hidden" class="address_id" value="{{$address->id}}" />
                                        <h5 class="text-green"><i class="fa fa-map-marker pr-2"></i> {{ucfirst($address->type)}}</h5>
                                        <p class="address-inner ml-4">{{$address->map_address}}
                                            <br>{{$Shop->estimated_delivery_time}}@lang('user.mins')</p>
                                        <p></p>

                                    </div>
                                </label>
                            </div>
                            @empty
                              
                            @endforelse
                            <!-- <div class="col-md-12 my-3 p-0">
                                <input type="radio" name="address-check" id="address1">
                                <label for="address1" class="radio">
                                    <div class="address-box p-3 br-10 box-shadow box">
                                        <h5 class="text-green"><i class="fa fa-map-marker pr-2"></i> Work</h5>
                                        <p class="address-inner ml-4">123 Road, ABC Street, Edison Avenue,<br>
                                            Baltimore, USA</p>
                                    </div>
                                </label>
                            </div> -->
                            <div class="col-md-12">
                                <a href="#" class="btn text-green pt-2" data-toggle="modal"
                                    data-target="#add_address"><i class="fa fa-plus plus-icon"></i>Add New Address</a>
                            </div>
                        </div>

                        <div class="payment mt-3">
                            <h5>Paytment Method</h5>
                            <?php $card_id = 0;?>
                            @forelse($cards as $card)
                            @if(@$card->is_default)
                            <?php $card_id = $card->id; ?>
                            <div class="col-md-12 mt-3 p-0 address">
                                <!-- <i class="fa fa-check select-item"></i> -->
                                
                                <!-- <input type="radio" name="payment-check" id="payment">
                                <label for="payment" class="radio"> -->

                                <!-- <input type="radio" class="form-control cvv default cardpay"  value="{{$card->id}}"  
                                                                 name="payment_method" id="card_{{$card->id}}"/>
                                                                <label class="pay-btn" for="card_{{$card->id}}"> @lang('pay') </label>
 -->

                                <input type="radio" class="cardpay"  value="{{$card->id}}"  
                                                                name="payment_method" id="card_{{$card->id}}"/>
                                <label class="radio" for="card_{{$card->id}}">
                                    <div class="card-box">
                                        <div class="new-card p-3"> 
                                            <div class=" d-flex align-items-center">
                                                <div class="card-img-section">
                                                    <img src="assets/images/login/mastercard.png" width="60%">
                                                </div>
                                                <div class="card-title-section">
                                                    <h5>XXX XXX XXXX {{$card->last_four}}</h5>
                                                    <p>Valid Until 03/21</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </label>
                            </div>
                            @else

                            <div class="col-md-12 mt-3 p-0 address">
                                <!-- <i class="fa fa-check select-item"></i> -->
                                
                                <!-- <input type="radio" name="payment-check" id="payment">
                                <label for="payment" class="radio"> -->

                                <input type="radio" class="cardpay"  value="{{$card->id}}"  
                                                                name="payment_method" id="card_{{$card->id}}"/>
                                <label class="radio" for="card_{{$card->id}}">

                                    <div class="card-box">
                                        <div class="new-card p-3"> 
                                            <div class=" d-flex align-items-center">
                                                <div class="card-img-section">
                                                    <img src="assets/images/login/mastercard.png" width="60%">
                                                </div>
                                                <div class="card-title-section">
                                                    <h5>XXX XXX XXXX {{$card->last_four}}</h5>
                                                    <p>Valid Until 03/21</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </label>
                            </div>
                            @endif
                            @empty
                            <div>@lang('home.payment.no_card')</div>
                            @endforelse
                            <div class="col-md-12">
                            <!-- <div href="#" data-toggle="modal" class="btn text-green pt-2" data-target="#card_model"><i class="fa fa-plus plus-icon"></i>Add New Card</a> -->

                                <a href="#" class="btn text-green pt-2" data-toggle="modal"
                                    data-target="#card_model"><i class="fa fa-plus plus-icon"></i>Add New Card</a>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane container" id="takeaway">
                        <h5>Date & Time</h5>
                        <form class="pt-2">
                            <div class="form-group">
                                <input type="date" class="form-control" name="">
                            </div>
                            <div class="form-group">
                                <input type="time" class="form-control" name="">
                            </div>
                        </form>

                        <div class="payment mt-3">
                            <h5>Paytment Method</h5>
                            <div class="col-md-12 mt-3 address">
                                <!-- <i class="fa fa-check select-item"></i> -->
                                <input type="radio" name="payment-check" id="payment2">
                                <label for="payment2" class="radio">
                                    <div class="card-box">
                                        <div class="new-card p-3">
                                            <div class=" d-flex align-items-center">
                                                <div class="card-img-section">
                                                    <img src="assets/images/login/mastercard.png" width="60%">
                                                </div>
                                                <div class="card-title-section">
                                                    <h5>***** 6527</h5>
                                                    <p>Valid Until 03/21</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <div class="col-md-12">
                                <div class="btn text-green pt-2" data-toggle="modal"
                                    data-target="#card_model"><i class="fa fa-plus plus-icon"></i>Add New Card</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>







            <div class="col-md-6 pt-3">
                <h4 class="py-3">Card Totals</h4>
                @if(count($Cart)>0)
                <form  action="{{url('orders')}}" id="order_checkout" method="POST">
                {{ csrf_field() }}
                <div class="table-responsive">
                    <table class="table table-bordered">
                    <thead>
                    </thead>
                    <tbody>
                    <?php $tot_gross=0;?>
                     @forelse($Cart['carts'] as $item)
                        <?php $tot_gross += $item->quantity*$item->product->prices->orignal_price;  ?>
                        <!-- <tr>
                            <th>Sub Total</th>
                            <td>{{ $tot_gross}}</td>
                        </tr> -->
                    @empty
                        <tr><td colspan="2">@lang('user.empty_cart')</td></tr>
                    @endforelse

                    </tbody>
                     <tfoot>
                        <!-- <tr>
                            <th>Tip</th>
                            <td>
                                <form class="radios">
                                    <label for="tips">
                                        <input type="radio" class="radio" name="tip"> 5%
                                    </label>
                                    <label for="tips">
                                        <input type="radio" class="radio" name="tip">15%
                                    </label>
                                    <div class="d-flex">
                                        <label for="tips">
                                            <input type="radio" class="radio" name="tip">
                                            <div class="value-button" id="decrease" onclick="decreaseValue()"
                                                value="Decrease Value">-</div>
                                            <input type="number" name="tip" id="number" value="30" />
                                            <div class="value-button" id="increase" onclick="increaseValue()"
                                                value="Increase Value">+</div>
                                        </label>
                                    </div>
                                </form>
                            </td>
                        </tr> -->



                        <tr>
                                            <td>@lang('user.item_total')</td>
                                            <td class="text-right sub-total">{{currencydecimal($tot_gross)}}</td>
                                            <input type="hidden" id="total_price" value="{{$tot_gross}}"/>
                                            <input type="hidden" id="total_addons_price" value="0"/>
                                            <input type="hidden" id="total_product_price" value="{{$tot_gross}}"/>
                                        </tr>

                                        <?php
                                     $net = $tot_gross;
                             $discount = 0;
                             if($Shop->offer_percent){
                                    if($tot_gross > $Shop->offer_min_amount){
                                       //$discount = roundPrice(($tot_price*($Order->shop->offer_percent/100)));
                                       $discount = ($tot_gross*($Shop->offer_percent/100));
                                       //if()
                                       $net = $tot_gross - $discount;
                                    }
                                }
                                $tax= $net*(Setting::get('tax')/100);

                                        ?>
                                        <tr>
                                            <td>Discount</td>
                                            <td class="text-right to_tax">- {{currencydecimal($discount)}}</td>
                                        </tr>

                                         <tr>
                                            <td>Tax</td>
                                            <td class="text-right to_tax">{{currencydecimal($tax)}}</td>
                                        </tr>
                                        <tr>
                                            <td>Delivery Charge</td>
                                            <td class="text-right to_tax">{{currencydecimal(Setting::get('delivery_charge'))}}</td>
                                        </tr>




                                        <?php 
                                                $net = $net+$tax+Setting::get('delivery_charge');
                                        ?>
                                        


                                       
                                        <tr class="removepromocode_msg">
                                        @if(Request::session()->has('promocode_id'))
                                        
                                            <td>@lang('user.used_promo')</td>
                                            <td></td>
                                            <td class="text-right">{{Request::session()->get('promocode_name')}} <a href="javascript:void(0)" class="removepromocode" data-id="{{Request::session()->get('promocode_id')}}"  >X</a></td>
                                        
                                        @endif
                                        </tr>
                                        <tr>
                                            <th>@lang('user.to_pay')</th>
                                            <th class="text-right to_pay">{{currencydecimal($net)}}</th>
                                             <input type="hidden" name="amount" id="total_amount_pay" value="{{$net}}" />
                                             <input type="hidden" name="ripple_price" id="ripple_price" value="0" />
                                             <input type="hidden" id="user_address_id" name="user_address_id" value="" />
                                             <input type="hidden" id="total_payment_mode" value="{{Setting::get('payment_mode')}}" name="payment_mode" />
                                            <input type="hidden" id="card_id" value="" name="card_id" />
                                            <input type="hidden" id="promocode_id" value="{{Request::session()->get('promocode_id')}}" name="promocode_id" />
                                            <input type="hidden" id="total_transaction_id" name="payment_id" />
                                            <input type="hidden" id="DestinationTag" name="DestinationTag" />
                                        </tr>
                                    </tfoot>
                                </table>




                                <!-- <button  type="submit" class="checkout-btn cart-main-btn  btn_checkout">@lang('user.proceed_to_pay')</button> -->
                        @else

                        @endif



                        <!-- <tr>
                            <th>Tax</th>
                            <td>$15.00</td>
                        </tr>
                        <tr>
                            <th>Total</th>
                            <td>$75.00</td>
                        </tr> -->
                    <!-- </table> -->
                </div>
            </div>





        </div>

        <div class="row">
            <div class="col-md-12">
                <button  type="submit" class="checkout-btn cart-main-btn  btn_checkout btn btn-green float-right">Place Order</button>

                <!-- <a class="btn btn-green float-right" href="#">Place Order</a> -->
            </div>
        </div>
        </form>
        <div>
			
				@if(Setting::get('payment_mode')=='braintree')
					@include('user.payment.partial.braintree')
				@else
					@include('user.payment.partial.stripe')
				@endif
		</div>
    </div>
</section>



<!--address modal-->




<!--Payment  modal-->

<div class="modal" id="add_payment" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-zoom modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <!-- Step1-->
                <div class="signin-step1">
                    <h4 class="form-header text-center py-2">Add Payment</h4>
                    <form>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <input type="text" name="" class="form-control" placeholder="Credit card Number">
                            </div>
                            <div class="col-md-6 form-group">
                                <input type="text" name="" class="form-control" placeholder="Expiry (MM/YY)">
                            </div>
                            <div class="col-md-6 form-group">
                                <input type="text" name="" class="form-control" placeholder="CVV">
                            </div>
                            <div class="col-md-12 form-group">
                                <input type="text" name="" class="form-control" placeholder="Name on Card">
                            </div>
                            <div class="col-md-12 form-group">
                                <input type="submit" name="" class="btn btn-green float-right form-control"
                                    Value="Add Card">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('styles')
<style type="text/css">
    /*#address {
	left:200px;
	top:500px;
	position:relative
	
}
*/
    .radio {
        cursor: pointer;
    }

    .radios input[type=radio] {
        content: '';
        background: #f4f4f4;
        border-radius: 100%;
        border: 1px solid #b4b4b4;
        display: inline-block;
        /* width: 1.4em;
  height: 1.4em;*/
        position: relative;
        /*top: -0.2em;
  margin-right: 1em;*/
        /*vertical-align: top;*/
        cursor: pointer;
        text-align: center;
        -webkit-transition: all 250ms ease;
        transition: all 250ms ease;
    }

    label {
        width: 100%;
    }

    /*.radios .radio{
    position: relative;
	display: block;
	background-color: #000000;
	color: #FFFFFF;
	left: 200px;
	top: 8px;
	width: 100px;
	height: 100px;
	line-height: 100px;
	text-align: center;
}*/
    .address input[type=radio] {
        display: none;
    }

    /*.address input[type=radio]:checked + .radio{
	background-color: #3ec423;
    color: #ffffff;
    border-radius: 10px;
}*/
    .address input[type=radio]:checked+.radio:before {
        font-family: FontAwesome;
        content: "\f00c";
        position: absolute;
        width: 30px;
        height: 30px;
        text-align: center;
        right: 15px;
        top: 15px;
        color: #fff;
        background: #3ec423;
        border-radius: 50%;
        padding: 5px;
    }

    /*.address input[type=radio]:checked + .radio .text-green{
    color: #ffffff;
}*/
</style>
@endsection

@section('deliveryscripts')
<script type="text/javascript">
$('.payment_mode_type').on('click',function(){
    var ctype = $(this).data('id');
    var addrs = $('#user_address_id').val();
    var card_id = $('#card_id').val();
    var p_mode = $('#total_payment_mode').val();
    if(ctype=='card'){
        if(addrs!='' && card_id!=''){
            $('#total_payment_mode').val("{{Setting::get('payment_mode')}}");
            $('.btn_checkout').prop('disabled',false);
            $('.chkouterrors').hide();
            $('.chkouterrors').html('');
        }else{
            
            $(this).prop('checked',false);
            $('.btn_checkout').prop('disabled',true);
            if(addrs ==''){
                $('.chkouterrors').removeClass('alert-success');
                $('.chkouterrors').addClass('alert-danger');
                $('.chkouterrors').show();
                $('.chkouterrors').html('please select address first');
            }
            if(card_id ==''){
                $('.chkouterrors').removeClass('alert-danger');
                $('.chkouterrors').addClass('alert-success');
                $('.chkouterrors').show();
                $('.chkouterrors').html('please select one card or add new card');
            }
        }
    }
    // if(ctype=='cash'){
    //     if(addrs!=''){
    //         $('#total_payment_mode').val("");
    //         $('#card_id').val("");
    //         $('.cardpay').prop('checked',false); 
    //         $('.btn_checkout').prop('disabled',false);
    //         $('.chkouterrors').hide();
    //         $('.chkouterrors').html('');
    //     }else{
    //         $(this).prop('checked',false);
    //         $('.btn_checkout').prop('disabled',true);
    //         $('.chkouterrors').removeClass('alert-success');
    //         $('.chkouterrors').addClass('alert-danger');
    //         $('.chkouterrors').show();
    //         $('.chkouterrors').html('please select address first');
    //     }
    // }
    // if(ctype=='ripple'){ 
    //     $('.btn_checkout').prop('disabled',true);
    //     if(addrs!=''){
    //         $('#total_payment_mode').val("ripple");
    //         $('#card_id').val("");
    //         var total_amount = $('#total_amount_pay').val();
    //         var now_1_xrp_usd = "{{@$ripple_response->last}}";

    //         var total_ripple = parseFloat(total_amount/now_1_xrp_usd);
    //         //alert(total_ripple);
    //         $('#t_xrp').html(total_ripple.toFixed(5));
    //         //$('.cardpay').prop('checked',false); 
    //         //$('.btn_checkout').prop('disabled',false);
    //         $('.chkouterrors').hide();
    //         $('.chkouterrors').html('');
    //     }else{
    //         $(this).prop('checked',false);
    //         $('.btn_checkout').prop('disabled',true);
    //         //$('.chkouterrors').removeClass('alert-success');
    //         $('.chkouterrors').addClass('alert-danger');
    //         $('.chkouterrors').show();
    //         $('.chkouterrors').html('please select address first');
    //     }
    // }
})

// $('.add_to_basket').click(function(){ 
//     var product_id = $(this).data('productid'); 
//     var quantity = $('#product_price_'+product_id).val(); 
//     var addons = '';
//     var cart_id = $(this).data('id'); 
//     var qty = 1;
//     addons = '';
//     if(cart_id){
//         qty = $('#add-quantity_'+cart_id).val();
//          addons +=' <input type="hidden" value="'+cart_id+'" name="cart_id" >';
//     }
//     $.ajax({
//         url: "{{url('/addons/')}}/"+product_id,
//         type:'GET',
//         success: function(data) { 
            
//              var p_price = qty*data.prices.orignal_price;
            
//              addons +=' <input type="hidden" value="'+data.shop_id+'" name="shop_id" >\
//                         <input type="hidden" value="'+data.id+'" name="product_id" >\
//                         <input type="hidden" value="'+qty+'" name="quantity" class="form-control" placeholder="Enter Quantity" min="1" max="100">\
//                         <input type="hidden" value="'+data.name+'" name="name" >\
//                         <input type="hidden" value="'+data.prices.orignal_price+'" name="price" />';
//             $.each( data.addons , function( key, value ) { 
//                 var chk='';
//                 if(cart_id){
//                     if($('#cart_addon_'+cart_id+'_'+value.id).val()){
//                         p_price = p_price+value.price;
//                         chk = "checked";
//                     }
//                 }
//                addons+='<div class="custom-block">\
//                                 <div class="row m-0">\
//                                     <img src="{{asset('assets/user/img/veg.jpg')}}" class="veg-icon">\
//                                     <div class="food-menu-details custom-details">\
//                                         <div class="form-check">\
//                                             <input class="form-check-input chkaddon" '+chk+' type="checkbox" name="product_addons['+value.id+']" value="'+value.id+'" id="addons-'+value.id+'"  data-price="'+value.price+'">\
//                                             <label class="form-check-label" for="addons-"'+value.id+'">'+value.addon.name+'({{Setting::get('currency')}}'+value.price.toFixed(2)+')</label>\
//                                              <input type="hidden" value="1" class=" input-number" name="addons_qty['+value.id+']"  />\
//                             <input type="hidden" name="addons_price['+value.id+']" value="'+value.price+'" />\
//                              <input type="hidden" name="addons_name['+value.id+']" value="'+value.addon.name+'" />\
//                                         </div>\
//                                     </div>\
//                                 </div>\
//                             </div>';
                
//             });
//             addons+='<input type="hidden" id="t_price" value="'+p_price+'"  >';
//             $('.p_name').html(data.name);
//             $('.p_price').html("{{Setting::get('currency')}}"+p_price.toFixed(2));
//             $('.t_price').html("Total {{Setting::get('currency')}}"+p_price.toFixed(2));
//              /*addons+='<div class="row">\
//                         <div class="col-md-4">\
//                             <label>Note</label>\
//                             </div>\
//                             <div class="col-md-8">\
//                             <textarea id="fullfilled" class="form-control counted" name="note" placeholder="Write Something" rows="5" style="margin-bottom:10px;" >\
//                             </textarea>\
//                         </div>\
//                     </div>';*/
//             $('#addon_list').html(addons);
//             $.each( data.addons , function( key, value ) {

//             });
//             $('#cart-custom-modal').modal('show');
//         },
//         error:function(jqXhr,status){ 
//             if( jqXhr.status === 422 ) {
//                 $(".print-error-msg").show();
//                 var errors = jqXhr.responseJSON; 

//                 $.each( errors , function( key, value ) { 
//                     $(".print-error-msg").find("ul").append('<li>'+value[0]+'</li>');
//                 });
//             } 
//         }
//     });
    
// })
$('.cardpay').on('click',function(){
    var id = $(this).val(); 
    $('#card_id').val(id);
    $('#total_payment_mode').val("stripe");
    $('.payment_mode_type.card').prop('checked',true);
    $('.btn_checkout').prop('disabled',false);
    $('.chkouterrors').hide();
    $('.chkouterrors').html('');
})
$(document).on('click','.chkaddon',function(){
    var price = $(this).data('price');
    if($(this).is(':checked')){
    var total_price = parseFloat($('#t_price').val()) + parseFloat(price);
    }else{
       var total_price = parseFloat($('#t_price').val()) - parseFloat(price); 
    }
    $('#t_price').val(total_price);
    $('.t_price').html('Total {{Setting::get("currency")}}'+total_price);
});

  $('.update_addr').on('click',function(){
   $('.addr_map').html($(this).children().find('.addr-map').text());
   $('.addr_type').html($(this).children().find('.addr-type').text());
   $('#user_address_id').val($(this).children().find('.address_id').val());
  })  
var error = 0;
function checkpayment(type){ 
    if($('#transaction_id+'+type).val() == ''){
        return false;
    }
    var myVar ;
      
        if(type=='eather'){
            var url = "{{url('checkEtherPayment')}}";
            var amount = (parseFloat($('#total_amount_pay').val())/{{@$ether_response->result->ethusd}}).toFixed(6); 
        }
        if(type=='ripple'){
            var url = "{{url('checkRipplePayment')}}";
            var amount = (parseFloat($('#total_amount_pay').val())/{{@$ripple_response->last}}).toFixed(6); 
        }
        if(type=='bitcoin'){
            var url = "{{url('checkBitcoinPayment')}}";
        }
    $.ajax({
      url: "https://data.ripple.com/v2/accounts/{{Setting::get('RIPPLE_KEY')}}/transactions?type=Payment&result=tesSUCCESS&limit=10",
      type: "GET",
      //data:{'payment_id':$('#transaction_id_'+type).val(),'amount':amount}
        })
      .done(function(response){ 
            console.log(response);
            var transa =1;
            var amount_ripple = $('#t_xrp').html();
            if((response.transactions).length > 0){

                $.each(response.transactions ,function(index,value){
                    
                    if(value.tx.DestinationTag == $('#transaction_id_'+type).val()){
                       transa =0;
                       console.log(value.tx.Amount/1000000 +'---'+ amount_ripple+'--'+parseFloat(amount_ripple - (value.tx.Amount/1000000)));
                       if(parseFloat(amount_ripple - (value.tx.Amount/1000000)) < 2){
                            $('#total_transaction_id').val(value.hash);
                            $('#DestinationTag').val(value.tx.DestinationTag);
                            $('#ripple_price').val(amount_ripple);
                            $("#"+type+"_form_error").html('Sucesss please proceed to Order');
                            $('.btn_checkout').prop('disabled',false);
                            $('#loader').hide();
                            $('#ripple_ord_btn').prop('disabled',false);
                            //$('#total_transaction_id').val($('#transaction_id_'+type).val());
                            //$('#total_payment_mode').val(type);
                            $('.default').prop('checked',false);
                        }else{
                            $("#"+type+"_form_error").html('Paid amount is less than the order amount. Please Repayment or contact with admin.');
                        }
                    }
                    

                });

                if(transa==1){
                    $('#loader').show();
                    $('#ord_btn').prop('disabled',true);
                    $("#"+type+"_form_error").html('Please Retry your Transaction is not found!');
                    //myVar = setTimeout(checkpayment('ripple'), 5000);
                }
                
            }
            else{
                $('#loader').show();
                $('#ord_btn').prop('disabled',true);
                $("#"+type+"_form_error").html('Please wait until your Transaction is Processing...');
                myVar = setTimeout(checkpayment('ripple'), 5000);
            }
            
        })
    .error(function(jqXhr,status){
        if(jqXhr.status === 422) {
            error =1;
            $("#ripple_form_error").html('');
            $("#ripple_form_error ").show();
            var errors = jqXhr.responseJSON;
            console.log(errors);
            $.each( errors , function( key, value ) { 
                $("#ripple_form_error").html(value);
            }); 
        } 
    
        $('#ord_btn').prop('disabled',true);
    })

    
}

function ClipBoard(id)
{
    var copyText = document.getElementById(id);

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");
}


$('.appcupon').click(function(){
    var code = $(this).data('code');
    var id = $(this).data('id');
    var price = $(this).data('price');
    $('#couponcode_apply').val(code);
    //$('#promocode_id').val(id);
    $('#couponcode_apply').data('price',price);
    $('#couponcode_apply').data('id',id);
})
var exist_promocode = "{{Request::session()->get('promocode_id')?:''}}";
$('.appcupon_apply').click(function(){
    if($('#couponcode_apply').val()!=''){
        var promocode_id = $('#couponcode_apply').data('id');
        var couponcode_apply = $('#couponcode_apply').val();
        
        if(exist_promocode){
                if(exist_promocode==promocode_id){
                    $('.message').html('Couponcode Already Added ');
                }else{
                    $('.message').html('Another Couponcode Already Added');
                }
        }else{
                $.ajax({
                    url: "{{url('wallet/promocode')}}",
                    type: "POST",
                    data:{'promocode_id':promocode_id,'check':1,'couponcode':couponcode_apply,'_token':"{{csrf_token()}}"}
                        })
                    .done(function(response){ 
                        exist_promocode = response.id;
                        $('#promocode_id').val(promocode_id);
                         var price = $('#couponcode_apply').data('price');
                        if(response.promocode_type=='amount'){
                            var tot_blnce = (parseFloat($('#total_amount_pay').val())-price).toFixed(2);
                        }else{
                            var tot_blnce = parseFloat(parseFloat($('#total_amount_pay').val()) -(parseFloat($('#total_amount_pay').val())*parseFloat(response.discount/100))).toFixed(2);
                        }
                         
                        $('#total_amount_pay').val(tot_blnce);
                        $('.to_pay').html(tot_blnce);
                        $('.message').html('Couponcode Added Successfully');
                        var promodetails='<td>Used Promocode</td>\
                                            <td></td>\
                                            <td class="text-right">'+response.promo_code+'\
                                            <a href="javascript:void(0)" class="removepromocode" data-id="'+response.id+'"  >X</a></td>';
                        $('.removepromocode_msg').show();
                        $('.removepromocode_msg').html(promodetails);
                        
                    })
                    .error(function(jqXhr,status){
                        if(jqXhr.status === 422) {
                            error =1;
                           
                            var errors = jqXhr.responseJSON;
                            console.log(errors);
                            $.each( errors , function( key, value ) { 
                                 $('.message').html(value);
                            }); 
                        } 
                    
                        $('#ord_btn').prop('disabled',true);
                    });
        }
    }else{
        $('.message').html('Please Select Any Coupon Code');
    }
    
})

$(document).on('click','.removepromocode',function(){
    var promocode_id = $(this).data('id');

    $.ajax({
        url: "{{url('wallet/promocode')}}",
        type: "POST",
        data:{'promocode_id':promocode_id,'check':1,'remove':1,'_token':"{{csrf_token()}}"}
            })
        .done(function(response){ 
            $('#promocode_id').val('');
             var price = response.discount;
             exist_promocode = '';
             console.log($('#total_amount_pay').val());
            if(response.promocode_type=='amount'){
                var tot_blnce = (parseFloat($('#total_amount_pay').val())+price).toFixed(2);
            }else{
                var tot_blnce = parseFloat(parseFloat($('#total_amount_pay').val())+(parseFloat($('#total_amount_netpay').val())*(response.discount/100))).toFixed(2);;
            }
             console.log(tot_blnce);
            $('#total_amount_pay').val(tot_blnce);
            $('.to_pay').html(tot_blnce);
            $('.removepromocode_msg').hide();
        })
        .error(function(jqXhr,status){
            if(jqXhr.status === 422) {
                error =1;
               
                var errors = jqXhr.responseJSON;
                console.log(errors);
                $.each( errors , function( key, value ) { 
                     $('.message').html(value);
                }); 
            } 
        
            $('#ord_btn').prop('disabled',true);
    });

})

/*setTimeout(function(){ 
        if(error ==0){
            checkpayment('');
        }
    }, 5000);*/
    </script>
    <script>
    $(document).ready(function() {
        function disableBack() { window.history.forward() }

        window.onload = disableBack();
        window.onpageshow = function(evt) { if (evt.persisted) disableBack() }
    });
</script>
@endsection