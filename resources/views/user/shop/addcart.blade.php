    @extends('user.layouts.app')

    @section('content')

    <section>
    	<div class="container">
    		<div class="row pt-5">
    			<div class="col-md-12 pt-5">
    				<div class="table-responsive">
    					<table class="table table-cart text-center">
    					<tbody>
    						<tr>
    							<th></th>
    							<th>Image</th>
    							<th>Product</th>
    							<th>Price</th>
    							<th>Quantity</th>
    							<th>Total</th>
    						</tr>
    						<?php $tot_gross=0;?>
                            @forelse($Cart['carts'] as $item)
    						<tr>


    							<td class="vl-m text-red">
    							<form action="{{ route('updatecart.destroy', $item->id) }}" method="POST">
                                	{{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="removefromcart" onclick="return confirm('Are you sure?')"><i class="fa fa-close fa-1x"></i></button>
                                </form>
    							</td>



    							<td><img src="{{@$item->product->images[0]->url}}" style="max-width:20%;" class="img-fluid"></td>
    							<td>{{@$item->product->name}}</td>


    							<td>
                                {{currencydecimal(@$item->product->prices->orignal_price)}}/each
    							</td>
    							<td>
    								<button class="cart-add-btn">
    									<div class="numbers-row" data-id="{{@$item->id}}" data-pid="{{@$item->product->id}}">
    									<input type="number" min="1" data-price="{{@$item->product->prices->orignal_price}}" name="add-quantity" class="add-sec" id="add-quantity_{{$item->id}}" value="{{$item->quantity}}">

    									</div>
    								</button>
    								
    							</td>

                                <td>
                                <?php $tot_gross += @$item->quantity*@$item->product->prices->orignal_price;  ?>
                                                    <p class="total_product_{{@$item->id}}">{{currencydecimal(@$item->quantity*@$item->product->prices->orignal_price)}}</p>
                                </td>
                                                    
                               
    						@empty
                            </tr>
                            <tr><td colspan="2">@lang('user.empty_cart')</td></tr>
                            @endforelse
    						<tr>
    							<td colspan="6">
    								<!-- <input type="text" name="" class="coupon-box"><a href="#" class="btn btn-green">Apply Coupon</a> -->
                                    <div class="form-group mb-0 apply-coupons">
            <div class="input-group mb-0">
              <input type="text"  name="" class="form-control coupon-box" placeholder="Enter Coupon Code" aria-label="Coupon Code">
              <div class="input-group-append">
                <button class="andro_btn-custom shadow-none" type="button">Apply</button>
              </div>
            </div>
          </div>
    								<a href="#" class="btn btn-green float-right">Update Cart</a>
    							</td>
    						</tr>
    					</tbody>
    					</table>
    				</div>
                    
    				<div class="col-md-6 offset-md-6 pt-3">
    					<h4 class="py-3">Card Totals</h4>
    					<div class="table-responsive">
    						<table class="table table-bordered">
    							<tr>
    								<th>Sub Total</th>

                                    <td class="sub-total text-right">{{currencydecimal(@$tot_gross)}}</td>

    							</tr>


                                <?php 
                                 $net = $tot_gross;
                                 $discount = 0;
                                 if($Shop->offer_percent){
                                        if($tot_gross > $Shop->offer_min_amount){
                                           //$discount = roundPrice(($tot_price*($Order->shop->offer_percent/100)));
                                           $discount = ($tot_gross*($Shop->offer_percent/100));
                                           //if()
                                           $net = $tot_gross - $discount;
                                        }
                                    }
                                    $tax= $net*(Setting::get('tax')/100);

                                            ?>
                                            <tr>
                                                <td>Discount</td>
                                                <td class="text-right to_tax">- {{currencydecimal($discount)}}</td>
                                            </tr>

                                             <tr>
                                                <td>Tax</td>
                                                <td class="text-right to_tax">{{currencydecimal($tax)}}</td>
                                            </tr>
                                            <tr>
                                                <td>Delivery Charge</td>
                                                <td class="text-right to_tax">{{currencydecimal(Setting::get('delivery_charge'))}}</td>
                                            </tr>




                                            <?php 
                                                    $net = $net+$tax+Setting::get('delivery_charge');
                                            ?>


    							<!-- <tr>
    								<th>Tip</th>
    								<td>
    									<form class="radios">
    										<label for="tips">
    											<input type="radio" name="tips"> 5% 
    										</label>
    										<label for="tips">
    											<input type="radio" name="tips">15%
    										</label>

    										<div class="d-flex">
    											<label for="tips">
    												<input type="radio" name="tips">
    												<div class="value-button" id="decrease" onclick="decreaseValue1()" value="Decrease Value">-</div>
    										  		<input type="number" id="number1" value="30" />
    										  		<div class="value-button" id="increase" onclick="increaseValue1()" value="Increase Value">+</div>
    										  	</label>
    									  	</div>
    									</form>
    								</td>
    							</tr>-->
    							<tr>
    								<th>Total</th>
                                    <td class="to_pay text-right">{{$net}}</td>
    							</tr>
    							
    						</table>
    					</div>
    				</div>
    			</div>
    		</div>
            <input type="hidden" id="total_price" value="{{$tot_gross}}"/>
    		<input type="hidden" id="total_addons_price" value="0"/>
    		<input type="hidden" id="total_product_price" value="{{$tot_gross}}"/>
    		<div class="row">
    			<div class="col-md-12">
    				<!-- <a href="#" class="btn btn-green float-right">Proceed to Checkout</a> -->
    				<a href="{{url('restaurant/details')}}?name={{$Shop->name}}&myaddress=home"  class="btn btn-green float-right">Proceed to Checkout <i class="ion-ios-arrow-thin-right"></i></a>

    			</div>
    		</div>
    	</div>
    </section>

    @endsection

        @section('styles')
        <style type="text/css">
            /* .active {
                background-color: Pink;
                :
    		} */
    		.cart-add-btn {
        padding: 0px 25px;
        line-height: inherit;
        border: 1px solid #d4d5d9;
        position: relative;
        height: 30px;
        background: #fff;
        font-size: 12px;
    }
    .add-sec {
        width: 30px;
        height: 28px;
        border: 0px;
        text-align: center;
    }
    .inc {
        position: absolute;
        top: 6px;
        right: 0px;
        text-align: center;
        width: 20px;
        height: 28px;
    }

    .dec {
        position: absolute;
        top: 6px;
        left: 0px;
        text-align: center;
        width: 20px;
        height: 28px;
    }

        </style>
        @endsection
    	@section('scripts')
    <!-- <script type="text/javascript" src="{{asset('assets/user/js/max-bootstrap.min.js')}}"></script> -->
    <script type="text/javascript">

    $(document).ready(function() {    
    $(document).on('click','.inc',function(e){
        e.preventDefault();
        var id = $(this).parent().attr('data-id'); 
        var pid = $(this).parent().attr('data-pid');   
        var input = $("input[id='add-quantity_"+id+"']");
        var currentVal = parseFloat(input.val());
        if (!isNaN(currentVal)) {
            product_price_calculation(id,'plus');
            changeCart(id,pid,currentVal);
        } else {
            input.val(0);
        }
    });
    $(document).on('click','.dec',function(e){
        e.preventDefault();
        var id = $(this).parent().attr('data-id');
        var pid = $(this).parent().attr('data-pid');    
        var input = $("input[id='add-quantity_"+id+"']");
        var currentVal = parseFloat(input.val());
        if (!isNaN(currentVal)) {
            if(currentVal==0){ 
                changeCart(id,pid,currentVal);
            }else{
            product_price_calculation(id,'minus'); 
            changeCart(id,pid,currentVal);  
            } 
        } else {
            input.val(0);
        }
    });

      function product_price_calculation(val,type){

        if(type == 'plus'){
          var qty = $('#add-quantity_'+val).val();

          var price = $('#add-quantity_'+val).data('price');
          var tot_amt = qty*price;
          $('.total_product_'+val).html("{{Setting::get('currency')}}"+tot_amt.toFixed(2));
          ///
          var total = parseFloat(price)+parseFloat($('#total_price').val());
          
          var total_product_price = parseFloat($('#total_product_price').val())+parseFloat(price);
          $('#total_product_price').val(total_product_price);
          var total_addons_price = $('#total_addons_price').val();
          total = parseFloat(total_product_price)+qty*parseFloat(total_addons_price);
          $('#total_price').val(total); 
          $('.sub-total').html("{{Setting::get('currency')}}"+total.toFixed(2));
        }else{
          var qty = $('#add-quantity_'+val).val();

          var price = $('#add-quantity_'+val).data('price');
          var tot_amt = qty*price;
          $('.total_product_'+val).html("{{Setting::get('currency')}}"+tot_amt.toFixed(2));
          ///
          var total = parseFloat(price)+parseFloat($('#total_price').val());
          
          var total_product_price = parseFloat($('#total_product_price').val())-parseFloat(price);
          $('#total_product_price').val(total_product_price);
          var total_addons_price = $('#total_addons_price').val();
          total = parseFloat(total_product_price)+qty*parseFloat(total_addons_price);
          $('#total_price').val(total.toFixed(2)); 
          $('.sub-total').html("{{Setting::get('currency')}}"+total.toFixed(2));
        }
    }
    });
    function changeCart(id,pid,qty){
        $.ajax({
            url: "{{url('addcart')}}",
            type:'POST',
            data:{'cart_id':id,'quantity':qty,'_token':"{{csrf_token()}}",'product_id':pid},
            success: function(res) { 
                console.log(res);
                $('.to_pay').html(res.net);
                $('.tax_net').html(res.tax);
                if(qty==0){
                location.reload();
                }
            },
            error:function(jqXhr,status){ 
                if( jqXhr.status === 422 ) {
                    $(".print-error-msg").show();
                    var errors = jqXhr.responseJSON; 

                    $.each( errors , function( key, value ) { 
                        $(".print-error-msg").find("ul").append('<li>'+value[0]+'</li>');
                    });
                } 
            }
        });
    }
    $('.shopfav').on('click',function(){
        if($(".shopfav i" ).hasClass( "active")){
            var url = "{{url('favourite')}}/{{$Shop->id}}";
            var method = 'DELETE';
        }else{
            var url = "{{url('favourite')}}";
            var method = 'POST';
        }
        $.ajax({
            url: url,
            type:method,
            data:{'shop_id':{{$Shop->id}},'_token':"{{csrf_token()}}"},
            success: function(res) { 
                if(method=='POST'){
                    $('.shopfav i').addClass('active');
                    $('.shopfav i').removeClass('ion-ios-heart-outline');
                    $('.shopfav i').addClass('ion-ios-heart');
                    $('.fav').html("Favourited");
                }else{
                    $('.shopfav i').removeClass('active');
                    $('.shopfav i').addClass('ion-ios-heart-outline');
                    $('.shopfav i').removeClass('ion-ios-heart');
                    $('.fav').html("Favourite");
                }
            },
            error:function(jqXhr,status){ 
                if( jqXhr.status === 422 ) {
                    $(".print-error-msg").show();
                    var errors = jqXhr.responseJSON; 

                    $.each( errors , function( key, value ) { 
                        $(".print-error-msg").find("ul").append('<li>'+value[0]+'</li>');
                    });
                } 
            }
        });
    });
    $('#veg-check').on('click',function(){
        if($(this).is(':checked')){
            $('#veg_check_form').submit();
        }else{
            $('#veg_check_form').submit();
        }
    })
    $('.prodsearch,.search-close').on('click',function(){
        $('#prod_search_form').submit();
    })
    document.onkeydown = function(evt) {
        evt = evt || window.event;
        if (evt.keyCode == 27) {
            $('#prod_search_form').submit();
        }
    };
    </script>
    @endsection
