@extends('user.layouts.app')

@section('content')
    <!-- <div class="container">
    <
    div class = "row" >
    <div class="col-md-8 col-md-offset-2">
            @include('include.alerts')
            <div class="panel panel-default">
                <div class="panel-heading">@lang('user.login')</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">@lang('user.create.phone')</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="+91xxxxxxxxxx" required autofocus>

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">@lang('user.create.password')</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> @lang('user.create.remember_me')
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    @lang('user.login')
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    @lang('forgot_your_password') ?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <
    /div> < /
div > -->

    <div id = "main-wrapper" class = "oxyy-login-register" >
    <div class="container-fluid px-0">
    <div class="row no-gutters min-vh-100"> 
      <!-- Login Form
      ========================= -->
      <div class="col-md-6 d-flex flex-column order-2 order-md-1">
            <div class="container my-auto py-5">
        <div class="row">
            <div class="col-11 col-md-10 col-lg-10 mx-auto">
                <h3 class="font-weight-60 text-12 mb-4">Sign In</h3>
                @include('include.alerts')
                <form id="loginForm" method="post" role="form" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <label class="font-weight-500" for="phone">@lang('user.create.phone')</label>
                    <div class="form-group  icon-group icon-group-right {{ $errors->has('phone') ? ' has-error' : '' }}">
                        <input class="form-control bg-light border-light" id="phone" type="text" name="phone" value="{{ old('phone') }}" placeholder="+91xxxxxxxxxx" required autofocus>
                        @if ($errors->has('phone'))
                        <span class="help-block">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                        @endif
                    </div>
                    <label class="font-weight-500" for="password">@lang('user.create.password')</label>
                    <div class="form-group icon-group icon-group-right {{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" class="form-control form-control-lg bg-light border-light" id="password" name="password" required>
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="row mt-4">
                        <div class="col-sm">
                            <div class="form-check custom-control custom-checkbox">
                                <input id="remember-me" class="custom-control-input" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label class="custom-control-label" for="remember-me">@lang('user.create.remember_me')</label>
                            </div>
                        </div>
                        <div class="col-sm text-right"><a class="btn-link" href="{{ route('password.request') }}">@lang('forgot_your_password') ?</a></div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-dark btn-lg btn-block my-4" type="submit"> @lang('user.login')</button>
                    </div>
                    <p class="text-2 text-muted text-center">Not a member? <a class="btn-link" href="#">Sign Up now</a></p>
                </form>
            </div>
        </div>
    </div>
        
      </div>
      <!-- Login Form End --> 
      
      <!-- Welcome Text
      ========================= -->
      <div class="col-md-6 order-1 order-md-2">
        <div class="hero-wrap d-flex align-items-center h-100">
          <div class="hero-mask opacity-8 bg-secondary"></div>
          <div class="hero-bg hero-bg-scroll" style="background-image:url('assets/img/lg-bg.jpg');"></div>
          <div class="hero-content mx-auto w-100 h-100 d-flex flex-column">
            <div class="row no-gutters my-auto py-5">
              <div class="col-10 col-lg-9 mx-auto">
                <p class="text-4 text-white">We are glad to see you again!</p>
                <h1 class="text-10 text-white mb-4">Join our next negotiation group in few minutes!</h1>
                <a class="btn btn-outline-light shadow-none video-btn mt-2" href="#" data-src="https://www.youtube.com/embed/7e90gBu4pas" data-toggle="modal" data-target="#videoModal"><span class="mr-2"><i class="fas fa-play-circle"></i></span>Watch demo</a> </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Welcome Text End --> 
    </div>
  </div> 
    </div>

@endsection