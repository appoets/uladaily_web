@extends('user.layouts.app')

@section('content')
<!-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('user.reset_password')</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">@lang('user.create.email')</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    @lang('send_password_reset_link')
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
 <div id = "main-wrapper" class = "oxyy-login-register" >
    <div class="container-fluid px-0">
    <div class="row no-gutters min-vh-100"> 
      <!-- Login Form
      ========================= -->
      <div class="col-md-6 d-flex flex-column order-2 order-md-1">
            <div class="container my-auto py-5">
        <div class="row">
            <div class="col-11 col-md-10 col-lg-10 mx-auto">
                <h3 class="font-weight-60 text-12 mb-4">Reset Password</h3>
                 @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                @include('include.alerts')
                <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}
                    <label class="font-weight-500" for="phone">@lang('user.create.email')</label>
                    <div class="form-group  form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email"  class="form-control bg-light border-light"name="email" value="{{ old('email') }}" required>
                        @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                    </div>
                    
                    
                    <div class="form-group">
                        <button class="btn btn-dark btn-lg btn-block my-4" type="submit">@lang('send_password_reset_link')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
        
      </div>
      <!-- Login Form End --> 
      
      <!-- Welcome Text
      ========================= -->
      <div class="col-md-6 order-1 order-md-2">
        <div class="hero-wrap d-flex align-items-center h-100">
          <div class="hero-mask opacity-8 bg-secondary"></div>
          <div class="hero-bg hero-bg-scroll" style="background-image:url('assets/img/lg-bg.jpg');"></div>
          <div class="hero-content mx-auto w-100 h-100 d-flex flex-column">
            <div class="row no-gutters my-auto py-5">
              <div class="col-10 col-lg-9 mx-auto">
                <p class="text-4 text-white">We are glad to see you again!</p>
                <h1 class="text-10 text-white mb-4">Join our next negotiation group in few minutes!</h1>
                </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Welcome Text End --> 
    </div>
  </div> 
    </div>
@endsection
