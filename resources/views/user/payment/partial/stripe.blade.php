
<div class="modal" id="card_model" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered modal-dialog-zoom modal-md" role="document">
    <div class="modal-content">
      <div class="modal-body">
<form id="payment-form" action="{{ route('card.store') }}" method="POST">
    {{ csrf_field() }}
    <div class="pt-5">
        <div class="radio d-flex align-items-center">
            <input id="radio-1" name="radio" type="radio" checked>
            <label for="radio-1" class="radio-label"><img src="assets/images/login/master.png" width="50%"></label>
            <input id="radio-2" name="radio" type="radio">
            <label for="radio-2" class="radio-label"><img src="assets/images/login/visa.png" width="50%"></label>
        </div>
    </div>
    <div class="pt-5" id="card-payment">
        <div class="row">
            <div class="col-md-12 form-group">
                <input data-stripe="number" type="text" onkeypress="return isNumberKey(event);" id="cardNumber" required
                    autocomplete="off" maxlength="19" class="form-control" placeholder="@lang('user.card.card_no')">
            </div>
            <div class="col-md-6 form-group">
                <input type="text" onkeypress="return isNumberKey(event);" maxlength="2" required autocomplete="off"
                    class="form-control" data-stripe="exp-month" placeholder="MM">
            </div>
            <div class="col-md-6 form-group">
                <input type="text" onkeypress="return isNumberKey(event);" maxlength="2" required autocomplete="off"
                    data-stripe="exp-year" class="form-control" placeholder="YY">
            </div>
            <div class="col-md-6 form-group">
                <input type="text" data-stripe="cvc" onkeypress="return isNumberKey(event);" required autocomplete="off"
                    maxlength="4" class="form-control" placeholder="@lang('user.card.cvv')">
            </div>
            <div class="col-md-6 form-group">
                <input data-stripe="name" autocomplete="off" required type="text" class="form-control"
                    placeholder="@lang('user.card.fullname')">
            </div>

        </div>
    </div>
    <div class="col-md-12 form-group">
        <button type="submit" class="btn btn-green float-right form-control">@lang('user.card.add_card')</button>

        <!-- <input type="submit" name="" class="btn btn-green float-right form-control" Value="Add Card"> -->
    </div>
</form>
</div>
</div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

<script type="text/javascript">
    Stripe.setPublishableKey("{{ Setting::get('stripe_publishable_key')}}");

    var stripeResponseHandler = function (status, response) {
        var $form = $('#payment-form');

        console.log(response);

        if (response.error) {
            // Show the errors on the form
            $form.find('.payment-errors').text(response.error.message);
            $form.find('button').prop('disabled', false);
            alert('error');

        } else {
            // token contains id, last4, and card type
            var token = response.id;
            console.log(token); // Insert the token into the form so it gets submitted to the server
            $form.append($('<input type="hidden" id="stripeToken" name="stripe_token" />').val(token));
            jQuery($form.get(0)).submit();
        }
    };

    $('#payment-form').submit(function (e) {

        if ($('#stripeToken').length == 0) {
            console.log('ok');
            var $form = $(this);
            $form.find('button').prop('disabled', true);
            console.log($form);
            Stripe.card.createToken($form, stripeResponseHandler);
            return false;
        }
    });
</script>
<script type="text/javascript">
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode != 46 && charCode > 31 &&
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    $('#card_pay').on('change', function () {

        if ($(this).is(':checked')) {
            $('$card_id').val($(this).val());
        }
    })

    $('#cardNumber').on('keyup', function (e) {
        var val = $(this).val();
        var newval = '';
        val = val.replace(/\s/g, '');
        for (var i = 0; i < val.length; i++) {
            if (i % 4 == 0 && i > 0) newval = newval.concat(' ');
            newval = newval.concat(val[i]);
        }
        $(this).val(newval);
    });
</script>