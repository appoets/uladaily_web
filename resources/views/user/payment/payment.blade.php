@extends('user.layouts.app')

@section('content')
@include('user.layouts.partials.user_common')

<div class="container">
	<div class="">
		@include('user.layouts.partials.sidebar')
		<div class="tab-content mb-5">

			<div class="tab-pane container active" id="payment">
				<div class="container">
					<div class="row">
						<div class=" col-md-8 offset-md-2">
							<div class="section-title">
								<h3>My Card</h3>
							</div>
						</div>
					</div>
					<div class="row card-block-1 pt-4">
						<div class=" col-md-8 offset-md-2">
							<div class="card-box">
								<div href="#" data-toggle="modal" data-target="#card_model">
									<div class="new-card p-3 card-next-1 ">
										<div class="d-flex align-items-center">
											<div class="card-img-section">
												<img src="assets/images/login/credit-card.png" width="60%">
											</div>
											<div class="card-title-section">
												<div class="pull-left">
													<span>@lang('user.create.foodie_money') :
														{{currencydecimal(Auth::user()->wallet_balance)}}</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row card-block-1 pt-4">
						<div class=" col-md-8 offset-md-2">
							<div class="card-box">
								<div href="#" data-toggle="modal" data-target="#card_model">
									<div class="new-card p-3 card-next-1 ">
										<div class="d-flex align-items-center">
											<div class="card-img-section">
												<img src="assets/images/login/credit-card.png" width="60%">
											</div>
											<div class="card-title-section">
												<h5> Add New Card</h5>
												<p>You may add upto 3 Cards (Visa or Mastercard) toyour Account</p>
											</div>
										</div>
									</div>
								</div>
							</div>


							<div class="card-box">
								<div class="new-card p-3">
									@forelse($cards as $card)
									@if(@$card->is_default)
									<?php $card_id = $card->id; ?>

									<form action="{{route('card.destroy',$card->id)}}" method="POST">
										{{ csrf_field() }}
										<input type="hidden" name="_method" value="DELETE">
										<input type="hidden" name="card_id" value="{{$card->card_id}}">
										<button onclick="return confirm('Are you sure?')" type="submit"
											class="remove-btn text-red float-right"><i class="fa fa-trash"></i></button>
										<a href="javascript:void(0);" data-toggle="modal" data-target="#wallet_modal" class="remove-btn text-red float-right mr-3"
											onclick="addmoney({{$card->id}});">@lang('user.create.add_money_in_wallet')</a>
									</form>

									<div class=" d-flex align-items-center">
										<div class="card-img-section">
											<img src="assets/images/login/mastercard.png" width="60%">
										</div>
										<div class="card-title-section">
											<h5>XXX XXX XXXX {{$card->last_four}}</h5>
											<p>Valid Until 03/21</p>
										</div>
									</div>
									@else

									<form action="{{route('card.destroy',$card->id)}}" method="POST">
										{{ csrf_field() }}
										<input type="hidden" name="_method" value="DELETE">
										<input type="hidden" name="card_id" value="{{$card->card_id}}">
										<button onclick="return confirm('Are you sure?')" type="submit"
											class="remove-btn text-red float-right"> <i
												class="fa fa-trash"></i></button>
										<div href="javascript:void(0);" data-toggle="modal" data-target="#wallet_modal" class="text-red float-right mr-3"
											onclick="addmoney({{$card->id}});">@lang('user.create.add_money_in_wallet')</div>
									</form>

									<div class=" d-flex align-items-center">
										<div class="card-img-section">
											<img src="assets/images/login/mastercard.png" width="60%">
										</div>
										<div class="card-title-section">
											<h5>XXX XXX XXXX {{$card->last_four}}</h5>
											<p>Valid Until 03/21</p>
										</div>
									</div>
									@endif
									@empty
									<div>@lang('home.payment.no_card')</div>
									@endforelse



								</div>
							</div>



						</div>
					</div>

					<div class="row card-block-2">
						<div class=" col-md-8 offset-md-2">


							@if(Setting::get('payment_mode')=='braintree')
							@include('user.payment.partial.braintree')
							@else
							@include('user.payment.partial.stripe')
							@endif



						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<!--Statement Modal Starts-->
<div class="modal" id="wallet_modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-dialog-centered modal-dialog-zoom modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Enter your Money</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<form class="edit-profile-section" action="{{ url('wallet') }}" method="POST">
					{{csrf_field()}}

					<div class="form-group">
						<label for="recipient-name" class="col-form-label">Amount:</label>

						<input type="number" name="amount" class="form-control" value="0">
						<input type="hidden" name="card_id" id="card_id" value="0" />
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Add</button>
					</div>
					</form>
					<!-- Edit form Section Ends -->
			</div>
		</div>
	</div>
	<!-- Login Content Ends -->
</div>
  
<script type="text/javascript">
       function addmoney(id){ 
    //     $('#wallet_modal').modal('show');
       $('#card_id').val(id);
         }
   </script>
 
<!-- Statement Modal Ends -->
@endsection