<header>
    <nav class="navbar navbar-expand-md navbar-dark top-header fixed-top">
        <div class="container">
            <a class="navbar-brand" href="javascript:void(0)"><img src="{{ asset(Setting::get('site_logo', 'logo.png')) }}" class="img-fluid" width="80%"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"> <span class="navbar-toggler-icon"></span> </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Right Side Of Navbar --><?php $setting = \App\Settings::where('key', 'CONTACT_NUMBER')->first(); ?>
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item"> <a class="nav-link" href="#"><i class="fa fa-phone"></i></a>2233456578</li> @if(Auth::guest())
                    <li class="nav-item"> <a class="nav-link" href="#" data-toggle="modal" data-target="#signup"><i class="fa fa-sign-in"></i>Sign Up</a> </li>
                    <li class="nav-item"> <a class="btn btn-green px-4" href="#" data-toggle="modal" data-target="#signin1">Login</a> </li>
                    <?php $cart_no =0; ?> @else
                        <?php 
                            $cart = \App\UserCart::list(Auth::user()->id);
                            $cart_no = count($cart);?>
                            <li>
                                <div class="select_language">
                                    <div class="dropdown text-xs-right ml-auto dropdown-toggle" data-toggle="dropdown" > <a class="" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-language" aria-hidden="true"></i>
                            <span>{{Auth::user()->name}}</span>
                        </a>
                                        <div class="dropdown-menu">
                                            <ul class="list-unstyled list-inline ct-topbar__list">
                                                <ul class="list-unstyled ct-language__dropdown">
                                                    <!-- <li>
                                    <a href="{{url('/orders')}}">Profile</a>
                                </li> -->
                                                    <li> <a href="{{url('/orders')}}">@lang('user.create.orders')</a> </li>
                                                    <li> <a href="{{url('/offers')}}">@lang('user.create.offers')</a> </li>
                                                    <li> <a href="{{url('/payments')}}">@lang('user.create.payments')</a> </li>
                                                    <li> <a href="{{url('/favourite')}}">@lang('user.create.fav')</a> </li>
                                                    <li> <a href="{{url('/useraddress')}}">@lang('user.create.address')</a> </li>
                                                    <li> <a href="{{url('/referral')}}">Referral</a> </li>
                                                    <li> <a href="javascript:void(0);" onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    @lang('menu.user.logout')</a> </li>
                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;"> {{ csrf_field() }} </form>
                                                </ul>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li> @endif
                            <?php 
                           if($cart_no==0){
                                $url = url('restaurant/details?name=');
                           }else{
                                $url = url('restaurant/details/addcart/'.@$cart[0]->product->shop->name);
                            }
                            ?> @if($cart_no > 0)
                                <li class="nav-item"> <a class="nav-link px-4" href="{{$url}}"><span class="cart-count">{{$cart_no}}</span> Cart</a> </li> @endif </ul>
                <div class="select_language">
                    <div class="dropdown text-xs-right ml-auto dropdown-toggle" data-toggle="dropdown"> <a class=""  href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-language" aria-hidden="true"></i>
                            <span>Select Language</span>
                        </a>
                        <div class="dropdown-menu">
                            <ul class="list-unstyled list-inline ct-topbar__list">
                                <ul class="list-unstyled ct-language__dropdown">
                                    <li><a href="" class="lang-en lang-select" data-lang="en">English</a></li>
                                </ul>
                            </ul>
                        </div>
                    </div>
                </div>
               <!--  <div class="dropdown select-country">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-language" aria-hidden="true"></i> <span>Select Country</span> <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="#" class="lang-en lang-select" data-lang="en">Canada</a></li>
                    </ul>
                </div> -->
                @if(Auth::guest())
                <!-- <div class="dropdown user-d-down">
                    <button class="btn btn-primary" type="button" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="assets/images/avatar.png" width="30px"> <span>Username</span> <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="#" class="lang-en lang-select" data-lang="en">Canada</a></li>
                    </ul>
                </div>
                <div class="cart-count-new">
                    <a href="#offcanvas-add-cart" class="offcanvas-toggle">
                                        <i class="fa fa-shopping-bag"></i>
                                        <span class="item-count">4</span>
                </a>
                </div> -->
                @endif
            </div>
        </div>
    </nav>
</header>