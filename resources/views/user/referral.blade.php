@extends('user.layouts.app')

@section('content')
<section class="profile-baner">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center mx-auto pt-5">
				<h4><b>Invite More <span class="text-green">Earn More</span></b></h4>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="refer-left">
					<div class="py-3 has-botom-border">
						<h6><b>$0 Earned. $50 Remaining</b></h6>
					</div>
					<div class="refer-count py-3">
						<h4 class="text-green"><b>New Invites</b></h4>
						<h2 class="text-green"><b>(0)</b></h2>
						<div class="progress my-3">
						  <div class="progress-bar" role="progressbar" aria-valuenow="70"
						  aria-valuemin="0" aria-valuemax="100" style="width:5%; background: #3ec423">
						    <span class="sr-only">10% Complete</span>
						  </div>
						</div>
						<p class="py-3">Invite 20 or more friends to Increase your Choices..</p>
					</div>
				</div>
			</div>
			<div class="col-md-7 col-sm-7 col-xs-12">
				<div class="refer-right">
					<div class="refer-top text-center has-botom-border">
						<h4><b>Earn $10 for Every friend</b></h4>
						<p>When you Introduce your friends to place their first</p>
					</div>
					<div class="refer-social has-botom-border">
						<h5 class="text-green py-3">Share your Link</h5>
						<p class="d-flex bordered align-items">
							<span class="text-light small-text">https:ezgrocery/d/123456</span>
							<span class="ml-auto float-right btn-copy">Copy</span>
						</p>
						<p class="d-flex bordered align-items">
							<span class="text-light small-text">Code:Johnsmith6524</span>
							<span class="ml-auto float-right btn-copy">Copy</span>
						</p>
						<div class="social-icon py-3">
							<h6><b>Share on:</b> <i class="fa fa-facebook f-bg"></i> <i class="fa fa-twitter t-bg"></i> <i class="fa fa-instagram i-bg"></i> <i class="fa fa-linkedin l-bg"></i></h6> 
						</div>
					</div>
					<div class="refer-email has-botom-border">
						<h5 class="text-green py-3">Send as Email Invite</h5>
						<p class="d-flex bordered align-items">
							<span class="text-light small-text">https:ezgrocery/d/123456</span>
						</p>
						<p class="d-flex bordered align-items">
							<span class="text-light small-text">https:ezgrocery/d/123456</span>
						</p>
						<p class="d-flex bordered align-items">
							<span class="text-light small-text">https:ezgrocery/d/123456</span>
						</p>
						<div class="d-flex py-3">
							<h6 class="text-green"><b>Preview Email</b></h6>
							<span class="ml-auto float-right btn-copy">Send</span>
						</div>
					</div>
					<div class="refer-contact text-center py-4">
						<h6 class="py-2 mb-3"><img src="assets/images/gmail.png" class="img-fluid mail-icon"><b>Share your love of food</b></h6>
						<span class="mx-auto btn-copy">Choose Contacts</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
