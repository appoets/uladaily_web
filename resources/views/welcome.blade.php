<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{Setting::get('site_title')}}</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{ Setting::get('site_favicon', asset('favicon.ico')) }}">
    <!-- Bootstrap CSS -->
    <link href="{{ asset('assets/user/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome CSS -->
    <link href="{{ asset('assets/user/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Ionicons CSS -->
    <link href="{{ asset('assets/user/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
    <!-- Material Icons CSS -->
    <link href="{{ asset('assets/user/material-icons/css/materialdesignicons.min.css')}}" rel="stylesheet">
    <!-- Slick CSS -->
    <link href="{{ asset('assets/user/css/slick.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/user/css/slick-theme.css')}}" rel="stylesheet">
    <!-- Style CSS -->
    <link href="{{ asset('assets/user/css/style.css')}}" rel="stylesheet">
    <style>
    .header-top {
        position: relative;
        z-index: 99999;
        background: #1d1c1c;
        font-size: 12px;
        width: 100%;
        padding: 11px 20px;
        color: #fff;
        text-align: center;
    }
    
    .header-top p {
        color: #fff;
        margin-bottom: 0px;
    }
    
    span.cross-icon.pull-right i {
        color: #fff;
    }
    
    span.cross-icon.pull-right i:hover {
        cursor: pointer;
    }
    
    .fixedmenu {
        top: 0px;
    }
    </style>
</head>

<body oncontextmenu="return false;" class="home landing-home"> @include('user.notification')
    <!--header starts-->
    <header>
        <div class="head_container container">
            <div class="logo"> <img src="{{ asset(Setting::get('site_logo', 'logo.png')) }}"> </div>
            <div class="menu" id="myTopnav">
            @if(Auth::guest())
                <ul> <a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="openNav()">&#9776;</a>
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li><a href="{{url('/shop')}}">Store</a></li>
                    <li><a href="{{url('login')}}">Login</a></li>
                    <li><a href="{{url('register')}}">Register</a></li>
                </ul>
                @endif
            </div>
        </div>
    </header>
    <div id="mySidenav" class="sidenav"> <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a> <a href="#">Home</a> <a href="#">About</a> <a href="#">Portfolio</a> <a href="#">Request Quote</a> <a href="#">Contact</a>
        <p style="color: #1c3664; text-align: center">Ula Daily &copy; 2020</p>
    </div>
    <!-- banner part starts -->
<section id="demos" class="demos-section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="section-header text-center">
                            <h2 class="section-title">
                                My Smart  <span>Basket </span>
                            </h2>
                            <p>
                                Order Online Grocery at Lowest Price Near You.
                            </p>
                        </div>
                        <div class="main-search-box">
                                
                                <!-- Main Search Input -->
                                <div class="main-search-input larger-input">
                                    <div class="banner-form">
                    <form  action="{{url('restaurants')}}" id="my_map_form" >
                                 
                                <div class="log-location-search input-group">
                                    <input type="text" id="pac-input ico-01 pac-target-input" id="autocomplete-input" name="search_loc" class="form-control" placeholder="@lang('user.delivery_location')" required autofocus>
                                    <span class="input-group-addon locate-me-btn my_map_form_current"><i class="ion-pinpoint"></i>   @lang('user.Locate_me')</span>
                                    <span class="input-group-addon log-search-btn"><button>@lang('user.find_food')</button></span>
                                </div>
                                <input type="hidden" id="latitude" name="latitude" value="{{ old('latitude') }}" readonly >
                                <input type="hidden" id="longitude" name="longitude" value="{{ old('longitude') }}" readonly >
                                <div id="my_map"   style="height:500px;width:500px;display: none" ></div>
                            </form>
                            <form  action="{{url('restaurants')}}" id="my_map_form_current" >
                                <input type="hidden" id="pac-input_cur" class="form-control search-loc-form" placeholder="Search for area,street name..." name="search_loc" value="{{ old('latitude') }}" >
                                <input type="hidden" id="latitude_cur" name="latitude" value="{{ old('latitude') }}" readonly >
                                <input type="hidden" id="longitude_cur" name="longitude" value="{{ old('longitude') }}" readonly >
                                
                            </form>
                </div>
                                </div>



                            </div>
                    </div>
                </div>
            </div>
            
            <div class="row justify-content-center">
                <div class="steps">
                    <div class="step-item step1">
                        <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 483 483" width="512" height="512">
                            <g fill="#fff">
                                <path d="M467.006 177.92c-.055-1.573-.469-3.321-1.233-4.755L407.006 62.877V10.5c0-5.799-4.701-10.5-10.5-10.5h-310c-5.799 0-10.5 4.701-10.5 10.5v52.375L17.228 173.164a10.476 10.476 0 0 0-1.22 4.938h-.014V472.5c0 5.799 4.701 10.5 10.5 10.5h430.012c5.799 0 10.5-4.701 10.5-10.5V177.92zM282.379 76l18.007 91.602H182.583L200.445 76h81.934zm19.391 112.602c-4.964 29.003-30.096 51.143-60.281 51.143-30.173 0-55.295-22.139-60.258-51.143H301.77zm143.331 0c-4.96 29.003-30.075 51.143-60.237 51.143-30.185 0-55.317-22.139-60.281-51.143h120.518zm-123.314-21L303.78 76h86.423l48.81 91.602H321.787zM97.006 55V21h289v34h-289zm-4.198 21h86.243l-17.863 91.602h-117.2L92.808 76zm65.582 112.602c-5.028 28.475-30.113 50.19-60.229 50.19s-55.201-21.715-60.23-50.19H158.39zM300 462H183V306h117v156zm21 0V295.5c0-5.799-4.701-10.5-10.5-10.5h-138c-5.799 0-10.5 4.701-10.5 10.5V462H36.994V232.743a82.558 82.558 0 0 0 3.101 3.255c15.485 15.344 36.106 23.794 58.065 23.794s42.58-8.45 58.065-23.794a81.625 81.625 0 0 0 13.525-17.672c14.067 25.281 40.944 42.418 71.737 42.418 30.752 0 57.597-17.081 71.688-42.294 14.091 25.213 40.936 42.294 71.688 42.294 24.262 0 46.092-10.645 61.143-27.528V462H321z"></path>
                                <path d="M202.494 386h22c5.799 0 10.5-4.701 10.5-10.5s-4.701-10.5-10.5-10.5h-22c-5.799 0-10.5 4.701-10.5 10.5s4.701 10.5 10.5 10.5z"></path>
                            </g>
                        </svg>
                        <h4><span>1. </span>Choose Store</h4> </div>
                    <!-- end:Step -->
                    <div class="step-item step2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewbox="0 0 380.721 380.721">
                            <g fill="#fff">
                                <path d="M58.727 281.236c.32-5.217.657-10.457 1.319-15.709 1.261-12.525 3.974-25.05 6.733-37.296a543.51 543.51 0 0 1 5.449-17.997c2.463-5.729 4.868-11.433 7.25-17.01 5.438-10.898 11.491-21.07 18.724-29.593 1.737-2.19 3.427-4.328 5.095-6.46 1.912-1.894 3.805-3.747 5.676-5.588 3.863-3.509 7.221-7.273 11.107-10.091 7.686-5.711 14.529-11.137 21.477-14.506 6.698-3.724 12.455-6.982 17.631-8.812 10.125-4.084 15.883-6.141 15.883-6.141s-4.915 3.893-13.502 10.207c-4.449 2.917-9.114 7.488-14.721 12.147-5.803 4.461-11.107 10.84-17.358 16.992-3.149 3.114-5.588 7.064-8.551 10.684-1.452 1.83-2.928 3.712-4.427 5.6a1225.858 1225.858 0 0 1-3.84 6.286c-5.537 8.208-9.673 17.858-13.995 27.664-1.748 5.1-3.566 10.283-5.391 15.534a371.593 371.593 0 0 1-4.16 16.476c-2.266 11.271-4.502 22.761-5.438 34.612-.68 4.287-1.022 8.633-1.383 12.979 94 .023 166.775.069 268.589.069.337-4.462.534-8.97.534-13.536 0-85.746-62.509-156.352-142.875-165.705 5.17-4.869 8.436-11.758 8.436-19.433-.023-14.692-11.921-26.612-26.631-26.612-14.715 0-26.652 11.92-26.652 26.642 0 7.668 3.265 14.558 8.464 19.426-80.396 9.353-142.869 79.96-142.869 165.706 0 4.543.168 9.027.5 13.467 9.935-.002 19.526-.002 28.926-.002zM0 291.135h380.721v33.59H0z" /> </g>
                        </svg>
                        <h4><span>2. </span>Order Groceries</h4> </div>
                    <!-- end:Step -->
                    <div class="step-item step3">
                        <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewbox="0 0 612.001 612">
                            <path d="M604.131 440.17h-19.12V333.237c0-12.512-3.776-24.787-10.78-35.173l-47.92-70.975a62.99 62.99 0 0 0-52.169-27.698h-74.28c-8.734 0-15.737 7.082-15.737 15.738v225.043h-121.65c11.567 9.992 19.514 23.92 21.796 39.658H412.53c4.563-31.238 31.475-55.396 63.972-55.396 32.498 0 59.33 24.158 63.895 55.396h63.735c4.328 0 7.869-3.541 7.869-7.869V448.04c-.001-4.327-3.541-7.87-7.87-7.87zM525.76 312.227h-98.044a7.842 7.842 0 0 1-7.868-7.869v-54.372c0-4.328 3.541-7.869 7.868-7.869h59.724c2.597 0 4.957 1.259 6.452 3.305l38.32 54.451c3.619 5.194-.079 12.354-6.452 12.354zM476.502 440.17c-27.068 0-48.943 21.953-48.943 49.021 0 26.99 21.875 48.943 48.943 48.943 26.989 0 48.943-21.953 48.943-48.943 0-27.066-21.954-49.021-48.943-49.021zm0 73.495c-13.535 0-24.472-11.016-24.472-24.471 0-13.535 10.937-24.473 24.472-24.473 13.533 0 24.472 10.938 24.472 24.473 0 13.455-10.938 24.471-24.472 24.471zM68.434 440.17c-4.328 0-7.869 3.543-7.869 7.869v23.922c0 4.328 3.541 7.869 7.869 7.869h87.971c2.282-15.738 10.229-29.666 21.718-39.658H68.434v-.002zm151.864 0c-26.989 0-48.943 21.953-48.943 49.021 0 26.99 21.954 48.943 48.943 48.943 27.068 0 48.943-21.953 48.943-48.943.001-27.066-21.874-49.021-48.943-49.021zm0 73.495c-13.534 0-24.471-11.016-24.471-24.471 0-13.535 10.937-24.473 24.471-24.473s24.472 10.938 24.472 24.473c0 13.455-10.938 24.471-24.472 24.471zm117.716-363.06h-91.198c4.485 13.298 6.846 27.54 6.846 42.255 0 74.28-60.431 134.711-134.711 134.711-13.535 0-26.675-2.045-39.029-5.744v86.949c0 4.328 3.541 7.869 7.869 7.869h265.96c4.329 0 7.869-3.541 7.869-7.869V174.211c-.001-13.062-10.545-23.606-23.606-23.606zM118.969 73.866C53.264 73.866 0 127.129 0 192.834s53.264 118.969 118.969 118.969 118.97-53.264 118.97-118.969-53.265-118.968-118.97-118.968zm0 210.864c-50.752 0-91.896-41.143-91.896-91.896s41.144-91.896 91.896-91.896c50.753 0 91.896 41.144 91.896 91.896 0 50.753-41.143 91.896-91.896 91.896zm35.097-72.488c-1.014 0-2.052-.131-3.082-.407L112.641 201.5a11.808 11.808 0 0 1-8.729-11.396v-59.015c0-6.516 5.287-11.803 11.803-11.803 6.516 0 11.803 5.287 11.803 11.803v49.971l29.614 7.983c6.294 1.698 10.02 8.177 8.322 14.469-1.421 5.264-6.185 8.73-11.388 8.73z" fill="#fff" /> </svg>
                        <h4><span>3. </span>Delivery</h4> </div>
                    <!-- end:Step -->
                </div>
                <!-- end:Steps -->
            </div>
</section>
    <!-- banner part ends -->
    <!-- Popular block starts -->
    <!-- <section class="popular">
        <div class="container">
            <div class="title text-xs-center m-b-30">
                <h2>Popular Dishes of the Month</h2>
                <p class="lead">The easiest way to your favourite food</p>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4 food-item">
                    <div class="food-item-wrap">
                        <div class="figure-wrap bg-image" style="background: url('/assets/user/images/product-1.jpg') center center / cover no-repeat;">
                            <div class="distance"><i class="fa fa-pin"></i>1240m</div>
                            <div class="rating pull-left"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </div>
                            <div class="review pull-right"><a href="#">198 reviews</a> </div>
                        </div>
                        <div class="content">
                            <h5><a href="dishes.php?res_id=48">Fresh Meats & Seafood</a></h5>
                            <div class="product-name">Chicken Cuts Lollipops Mince, Fish Other Seafood, Fresh Kebabs Others</div>
                            <div class="price-btn-block"> <span class="price">$55.77</span> <a href="dishes.php?res_id=48" class="btn theme-btn-dash pull-right">Order Now</a> </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 food-item">
                    <div class="food-item-wrap">
                        <div class="figure-wrap bg-image" style="background: url('/assets/user/images/product-2.jpg') center center / cover no-repeat;">
                            <div class="distance"><i class="fa fa-pin"></i>1240m</div>
                            <div class="rating pull-left"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </div>
                            <div class="review pull-right"><a href="#">198 reviews</a> </div>
                        </div>
                        <div class="content">
                            <h5><a href="dishes.php?res_id=48">Fresh Meats & Seafood</a></h5>
                            <div class="product-name">Chicken Cuts Lollipops Mince, Fish Other Seafood, Fresh Kebabs Others</div>
                            <div class="price-btn-block"> <span class="price">$55.77</span> <a href="dishes.php?res_id=48" class="btn theme-btn-dash pull-right">Order Now</a> </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 food-item">
                    <div class="food-item-wrap">
                        <div class="figure-wrap bg-image" style="background: url('/assets/user/images/product-3.jpg') center center / cover no-repeat;">
                            <div class="distance"><i class="fa fa-pin"></i>1240m</div>
                            <div class="rating pull-left"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </div>
                            <div class="review pull-right"><a href="#">198 reviews</a> </div>
                        </div>
                        <div class="content">
                            <h5><a href="dishes.php?res_id=48">Fresh Meats & Seafood</a></h5>
                            <div class="product-name">Chicken Cuts Lollipops Mince, Fish Other Seafood, Fresh Kebabs Others</div>
                            <div class="price-btn-block"> <span class="price">$55.77</span> <a href="dishes.php?res_id=48" class="btn theme-btn-dash pull-right">Order Now</a> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->

        <!-- How it works block ends -->
    <div class="boost-your-phone">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-xs-12 float-right  wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
                                <div class="text">
                                    <div class="theme-title-two">
                                        <h2>Grocery has gone digital & now is the time to act</h2>
                                        <p>Exploding demand for online grocery requires a seamless strategy, marked by an immersive brand experience & a high level of convenience and personalization.</p>
                                        <p>We has the omni-channel solutions you need to profitably grow your business.</p>
                                        <p>Click to learn more about our products, services, and technology!</p>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="boster-image-wrapper  wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                            <img src="" alt="" class="try">
                            <input type="checkbox" id="ao-toggle" class="ao-toggle" name="ao-toggle">
                            <img src="assets/user/images/shape-2.png" alt="">
                            <div class="ao-annotations">
                                <span>Full Localisation Support</span>
                                <span>Custom Image Widget</span>
                                <span>Blog and Contact Widgets</span>
                                <span>Easy Theme Options</span>
                                <span>4 Footer Widget Columns</span>
                            </div>
                        </div> <!-- /.boster-image-wrapper -->
                    </div> <!-- /.container -->
    </div>
    <!-- Popular block ends -->
    <!-- How it works block starts -->
    <section class="how-it-works">
        <div class="container">
            <div class="text-xs-center">
                <h2>Easy 3 Step Order</h2>
                <!-- 3 block sections starts -->
                <div class="row how-it-works-solution">
                    <div class="col-xs-12 col-sm-12 col-md-4 how-it-works-steps white-txt col1">
                        <div class="how-it-works-wrap">
                            <div class="step step-1">
                                <div class="icon" data-step="1">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 483 483" width="512" height="512">
                                        <g fill="#FFF">
                                            <path d="M467.006 177.92c-.055-1.573-.469-3.321-1.233-4.755L407.006 62.877V10.5c0-5.799-4.701-10.5-10.5-10.5h-310c-5.799 0-10.5 4.701-10.5 10.5v52.375L17.228 173.164a10.476 10.476 0 0 0-1.22 4.938h-.014V472.5c0 5.799 4.701 10.5 10.5 10.5h430.012c5.799 0 10.5-4.701 10.5-10.5V177.92zM282.379 76l18.007 91.602H182.583L200.445 76h81.934zm19.391 112.602c-4.964 29.003-30.096 51.143-60.281 51.143-30.173 0-55.295-22.139-60.258-51.143H301.77zm143.331 0c-4.96 29.003-30.075 51.143-60.237 51.143-30.185 0-55.317-22.139-60.281-51.143h120.518zm-123.314-21L303.78 76h86.423l48.81 91.602H321.787zM97.006 55V21h289v34h-289zm-4.198 21h86.243l-17.863 91.602h-117.2L92.808 76zm65.582 112.602c-5.028 28.475-30.113 50.19-60.229 50.19s-55.201-21.715-60.23-50.19H158.39zM300 462H183V306h117v156zm21 0V295.5c0-5.799-4.701-10.5-10.5-10.5h-138c-5.799 0-10.5 4.701-10.5 10.5V462H36.994V232.743a82.558 82.558 0 0 0 3.101 3.255c15.485 15.344 36.106 23.794 58.065 23.794s42.58-8.45 58.065-23.794a81.625 81.625 0 0 0 13.525-17.672c14.067 25.281 40.944 42.418 71.737 42.418 30.752 0 57.597-17.081 71.688-42.294 14.091 25.213 40.936 42.294 71.688 42.294 24.262 0 46.092-10.645 61.143-27.528V462H321z" />
                                            <path d="M202.494 386h22c5.799 0 10.5-4.701 10.5-10.5s-4.701-10.5-10.5-10.5h-22c-5.799 0-10.5 4.701-10.5 10.5s4.701 10.5 10.5 10.5z" /> </g>
                                    </svg>
                                </div>
                                <h3>Choose a Store</h3>
                                <p>We"ve got your covered with menus from over 345 delivery restaurants online.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 how-it-works-steps white-txt col2">
                        <div class="step step-2">
                            <div class="icon" data-step="2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewbox="0 0 380.721 380.721">
                                    <g fill="#FFF">
                                        <path d="M58.727 281.236c.32-5.217.657-10.457 1.319-15.709 1.261-12.525 3.974-25.05 6.733-37.296a543.51 543.51 0 0 1 5.449-17.997c2.463-5.729 4.868-11.433 7.25-17.01 5.438-10.898 11.491-21.07 18.724-29.593 1.737-2.19 3.427-4.328 5.095-6.46 1.912-1.894 3.805-3.747 5.676-5.588 3.863-3.509 7.221-7.273 11.107-10.091 7.686-5.711 14.529-11.137 21.477-14.506 6.698-3.724 12.455-6.982 17.631-8.812 10.125-4.084 15.883-6.141 15.883-6.141s-4.915 3.893-13.502 10.207c-4.449 2.917-9.114 7.488-14.721 12.147-5.803 4.461-11.107 10.84-17.358 16.992-3.149 3.114-5.588 7.064-8.551 10.684-1.452 1.83-2.928 3.712-4.427 5.6a1225.858 1225.858 0 0 1-3.84 6.286c-5.537 8.208-9.673 17.858-13.995 27.664-1.748 5.1-3.566 10.283-5.391 15.534a371.593 371.593 0 0 1-4.16 16.476c-2.266 11.271-4.502 22.761-5.438 34.612-.68 4.287-1.022 8.633-1.383 12.979 94 .023 166.775.069 268.589.069.337-4.462.534-8.97.534-13.536 0-85.746-62.509-156.352-142.875-165.705 5.17-4.869 8.436-11.758 8.436-19.433-.023-14.692-11.921-26.612-26.631-26.612-14.715 0-26.652 11.92-26.652 26.642 0 7.668 3.265 14.558 8.464 19.426-80.396 9.353-142.869 79.96-142.869 165.706 0 4.543.168 9.027.5 13.467 9.935-.002 19.526-.002 28.926-.002zM0 291.135h380.721v33.59H0z" /> </g>
                                </svg>
                            </div>
                            <h3>Choose a product</h3>
                            <p>We"ve got your covered with menus from over 30+ grocery items online.</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 how-it-works-steps white-txt col3">
                        <div class="step step-3">
                            <div class="icon" data-step="3">
                                <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewbox="0 0 612.001 612">
                                    <path d="M604.131 440.17h-19.12V333.237c0-12.512-3.776-24.787-10.78-35.173l-47.92-70.975a62.99 62.99 0 0 0-52.169-27.698h-74.28c-8.734 0-15.737 7.082-15.737 15.738v225.043h-121.65c11.567 9.992 19.514 23.92 21.796 39.658H412.53c4.563-31.238 31.475-55.396 63.972-55.396 32.498 0 59.33 24.158 63.895 55.396h63.735c4.328 0 7.869-3.541 7.869-7.869V448.04c-.001-4.327-3.541-7.87-7.87-7.87zM525.76 312.227h-98.044a7.842 7.842 0 0 1-7.868-7.869v-54.372c0-4.328 3.541-7.869 7.868-7.869h59.724c2.597 0 4.957 1.259 6.452 3.305l38.32 54.451c3.619 5.194-.079 12.354-6.452 12.354zM476.502 440.17c-27.068 0-48.943 21.953-48.943 49.021 0 26.99 21.875 48.943 48.943 48.943 26.989 0 48.943-21.953 48.943-48.943 0-27.066-21.954-49.021-48.943-49.021zm0 73.495c-13.535 0-24.472-11.016-24.472-24.471 0-13.535 10.937-24.473 24.472-24.473 13.533 0 24.472 10.938 24.472 24.473 0 13.455-10.938 24.471-24.472 24.471zM68.434 440.17c-4.328 0-7.869 3.543-7.869 7.869v23.922c0 4.328 3.541 7.869 7.869 7.869h87.971c2.282-15.738 10.229-29.666 21.718-39.658H68.434v-.002zm151.864 0c-26.989 0-48.943 21.953-48.943 49.021 0 26.99 21.954 48.943 48.943 48.943 27.068 0 48.943-21.953 48.943-48.943.001-27.066-21.874-49.021-48.943-49.021zm0 73.495c-13.534 0-24.471-11.016-24.471-24.471 0-13.535 10.937-24.473 24.471-24.473s24.472 10.938 24.472 24.473c0 13.455-10.938 24.471-24.472 24.471zm117.716-363.06h-91.198c4.485 13.298 6.846 27.54 6.846 42.255 0 74.28-60.431 134.711-134.711 134.711-13.535 0-26.675-2.045-39.029-5.744v86.949c0 4.328 3.541 7.869 7.869 7.869h265.96c4.329 0 7.869-3.541 7.869-7.869V174.211c-.001-13.062-10.545-23.606-23.606-23.606zM118.969 73.866C53.264 73.866 0 127.129 0 192.834s53.264 118.969 118.969 118.969 118.97-53.264 118.97-118.969-53.265-118.968-118.97-118.968zm0 210.864c-50.752 0-91.896-41.143-91.896-91.896s41.144-91.896 91.896-91.896c50.753 0 91.896 41.144 91.896 91.896 0 50.753-41.143 91.896-91.896 91.896zm35.097-72.488c-1.014 0-2.052-.131-3.082-.407L112.641 201.5a11.808 11.808 0 0 1-8.729-11.396v-59.015c0-6.516 5.287-11.803 11.803-11.803 6.516 0 11.803 5.287 11.803 11.803v49.971l29.614 7.983c6.294 1.698 10.02 8.177 8.322 14.469-1.421 5.264-6.185 8.73-11.388 8.73z" fill="#FFF" /> </svg>
                            </div>
                            <h3>Pick up or Delivery</h3>
                            <p>Get your food delivered! And enjoy your day! Pay online on pickup or delivery</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- 3 block sections ends -->
        </div>
    </section>



      <section class="works" style="background: #f0f5f9;">
    <div class="container">
       <div class="row">
        <div class="col-md-12">
          <div class="section-title text-center">
            <img src="assets/images/title-icon.png" class="img-fluid title-img">
            <h2 class="text-uppercase">How it <span class="text-green">Works</span></h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="works-list">
             <ul>
                <li>
                   <span>
                     <img src="assets/images/work-1.png">
                   </span>
                   <h6>Enter your location</h6>
                </li>
                <li>
                   <span>
                      <img src="assets/images/work-2.png">
                   </span>
                   <h6>Select Desired Store</h6>
                </li>
                <li>
                   <span>
                      <img src="assets/images/work-3.png">
                   </span>
                   <h6>Shop your Grocery</h6>
                </li>
                <li>
                   <span>
                      <img src="assets/images/work-4.png">
                   </span>
                   <h6>Select Delivery Option</h6>
                </li>
                <li>
                   <span>
                      <img src="assets/images/work-5.png">
                   </span>
                   <h6>Grocery at your Doorstep</h6>
                </li>
             </ul>
          </div>
      </div>
    </div>
  </section>
    <!-- call to action -->
    <section class="food-apps">
        <div class="bg-shape-style"></div>
        <div class="container">
            <div class="row align-items-center flex-row-reverse">
                <div class="col-lg-6 col-12">
                    <div class="apps-content padding-tb">
                        <div class="section-header">
                            <h3>Fresh produce picked <br>perfectly</h3>
                            <p>We know how to pick the freshest produce with the perfect ripeness. And we'll keep your eggs safe too.</p>
                            <div class="action-btns download-btn mt-4">
                                <a href="#" class="btn solid-white-btn mr-3"> <span class="fa fa-apple mr-2"></span> App Store</a>
                                <a href="#" class="btn outline-white-btn"> <span class="fa fa-android mr-2"></span> Play Store</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="apps-thumb"> <img src="assets/user/images/mock.png" alt="food-apps"> </div>
                </div>
            </div>
        </div>
    </section>
    <!-- call to action ends here -->
    <!-- start: FOOTER -->
    <footer class="footer">
        <div class="container">
            <!-- top footer statrs -->
            <div class="row top-footer">
                <div class="col-xs-12 col-sm-3 footer-logo-block color-gray">
                    <a href="#"> <img src="/assets/user/images/logo.png" alt="Footer logo"> </a> <span>Order Delivery &amp; Take-Out </span> </div>
                <div class="col-xs-12 col-sm-2 about color-gray">
                    <h5>About Us</h5>
                    <ul>
                        <li><a href="#">About us</a> </li>
                        <li><a href="#">History</a> </li>
                        <li><a href="#">Our Team</a> </li>
                        <li><a href="#">We are hiring</a> </li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-2 how-it-works-links color-gray">
                    <h5>How it Works</h5>
                    <ul>
                        <li><a href="#">Enter your location</a> </li>
                        <li><a href="#">Choose Store</a> </li>
                        <li><a href="#">Choose Grocery</a> </li>
                        <li><a href="#">Pay via credit card</a> </li>
                        <li><a href="#">Wait for delivery</a> </li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-2 pages color-gray">
                    <h5>Pages</h5>
                    <ul>
                        <li><a href="#">Search results page</a> </li>
                        <li><a href="#">User Sing Up Page</a> </li>
                        <li><a href="#">Pricing page</a> </li>
                        <li><a href="#">Make order</a> </li>
                        <li><a href="#">Add to cart</a> </li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-3 popular-locations color-gray">
                    <h5>Popular locations</h5>
                    <ul>
                        <li><a href="#">Sarajevo</a> </li>
                        <li><a href="#">Split</a> </li>
                        <li><a href="#">Tuzla</a> </li>
                        <li><a href="#">Sibenik</a> </li>
                        <li><a href="#">Zagreb</a> </li>
                        <li><a href="#">Brcko</a> </li>
                        <li><a href="#">Beograd</a> </li>
                        <li><a href="#">New York</a> </li>
                        <li><a href="#">Gradacac</a> </li>
                        <li><a href="#">Los Angeles</a> </li>
                    </ul>
                </div>
            </div>
            <!-- top footer ends -->
        </div>
    </footer>
    <!-- end:Footer -->


    <script src="{{ asset('assets/user/js/jquery.min.js')}}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ asset('assets/user/js/bootstrap.min.js')}}"></script>
    <!-- Slick Slider JS -->
    <script src="{{ asset('assets/user/js/slick.min.js')}}"></script>
    <!-- Sidebar JS -->
    <script src="{{ asset('assets/user/js/asidebar.jquery.js')}}"></script>
    <!-- Map JS -->
    <script src="https://maps.googleapis.com/maps/api/js?key={{Setting::get('GOOGLE_API_KEY')}}></script>
    <script src=" {{ asset( 'assets/user/js/jquery.googlemap.js')}} "></script>
    <!-- Incrementing JS -->
    <script src="{{ asset( 'assets/user/js/incrementing.js')}} "></script>
    <!-- Scripts -->
    <script src="{{ asset( 'assets/user/js/scripts.js')}} "></script>
    @include('user.layouts.partials.script')
    <!-- Start of LiveChat (www.livechatinc.com) code -->
    @if(Setting::get('DEMO_MODE') == 0)

         <script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 10555997;
(function() {
var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<noscript>
<a href="https://www.livechatinc.com/chat-with/10555997/" rel="nofollow">Chat with us</a>,
powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a>
</noscript>
           @endif

          <!-- End of LiveChat code -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
             <!--  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113150309-2 "></script>
              <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());

                gtag('config', 'UA-113150309-2');
             </script>   -->



<script type="text/javascript ">
    $(document).bind("contextmenu ",function(e) {
 e.preventDefault();
});
$(document).keydown(function(e){
    console.log(e);
    if(e.which === 123){
       return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
     return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
     return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
     return false;
    }
    if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
     return false;
    }
});
</script>
<script>
$(document).ready(function()
{
$('span.cross-icon').click(function()
{
$('.header-top').slideUp();
$('.navbar').css('top','0px');
});

});
$(window).scroll(function()
{
if($(this).scrollTop()>50)
{
$('header>nav.navbar.navbar-fixed-top').addClass('fixedmenu')
}
else{
$('header>nav.navbar.navbar-fixed-top').removeClass('fixedmenu');
}
});
</script>
<script type="text/javascript ">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');

}
</script>
<script type="text/javascript ">
    
function openNav() {
    document.getElementById("mySidenav ").style.width = "250px ";
    //document.getElementById("header ").style.marginLeft = "250px ";
}

function closeNav() {
    document.getElementById("mySidenav ").style.width = "0 ";
    //document.getElementById("header ").style.marginLeft= "0 ";
};
</script>

<script type="text/javascript " src="{{ asset( 'assets/translate/f.txt')}} "></script>
<script type="text/javascript">
  // Categories Slider
$('.promo-slider').slick({
  slidesToShow: 3,
  arrows: true,
  dots: true,
  infinite: true,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }
  ]
});

</script>
</body>

</html>