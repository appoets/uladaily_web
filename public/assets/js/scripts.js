$(document).ready(function() {

    var $window = $(window);

    function setCSS() {
        var windowHeight = $(window).height();
        var windowWidth = $(window).width();

        var disRight = $(".dis-right").outerHeight(true);
        var pageHeader = $(".page-header").outerHeight(true);
        var orderlistSection = (windowHeight - pageHeader);

        // $('#basic-map').height( $(window).height() - 200 );
        $('.dis-left').css('height', disRight);
        $('.win-height').css('height', orderlistSection);
        $('.login').css('min-height', windowHeight);

    };

    setCSS();
    $(window).on('load resize', function() {
        setCSS();
    });
});

$('.intro-slide').slick({
    dots: false,
    arrows: true,
    infinite: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                autoplay: true
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});

// Dipute JQuery
$(".order-box").click(function() {
    if ($('.order-box').hasClass('active')) {
        $('.order-box').removeClass('active'),
            $(this).addClass('active');
    }
});